package net.Archeantus.Zombinator.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import net.Archeantus.Zombinator.Zombinator;
import net.Archeantus.Zombinator.Audio.Audio;

public class KeyboardInput implements KeyListener{
	
	private Zombinator z;
	
	public KeyboardInput(Zombinator z){this.z = z;}
	
	@Override
	public void keyPressed(KeyEvent ke) {
		switch(ke.getKeyCode()){
			case KeyEvent.VK_A:
				if(!z.p.isMoving() && z.ingame && z.p.isAlive()){z.p.moveLeft();z.walking.play(true);}
				break;
			case KeyEvent.VK_D:
				if(!z.p.isMoving() && z.ingame && z.p.isAlive()){z.p.moveRight();z.walking.play(true);}
				break;
			case KeyEvent.VK_E:
				if(z.ingame && z.p.getBounds().intersects(z.c.getBounds(z.offset)) && z.p.isAlive()){if(z.coins >= z.price){z.c.toggle();z.coins -= z.price;z.g.randomizedGun();Audio.playStoredSound("random", 0.2, 0);}}
				break;
			case KeyEvent.VK_SPACE:
				if(z.p.isAlive()){z.p.Jump();}
				break;
			case KeyEvent.VK_ENTER:
				if(z.p.isAlive()){z.g.Shoot();}
				break;
			case KeyEvent.VK_SHIFT:
				if(z.p.isAlive()){z.g.Switch();}
				break;
			case KeyEvent.VK_R:
				if(z.p.isAlive()){z.p.setReloading(true);z.reload = true;}
				break;
		}
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		switch(ke.getKeyCode()){
			case KeyEvent.VK_A:
				if(z.p.isMoving()){z.p.stopMoving();z.walking.pause();}
				break;
			case KeyEvent.VK_D:
				if(z.p.isMoving()){z.p.stopMoving();z.walking.pause();}
				break;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
