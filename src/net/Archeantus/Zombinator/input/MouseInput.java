package net.Archeantus.Zombinator.input;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import net.Archeantus.Zombinator.Zombinator;
import net.Archeantus.Zombinator.Audio.Audio;
import net.Archeantus.Zombinator.Entities.GunItem;
import net.Archeantus.Zombinator.Entities.Player;
import net.Archeantus.Zombinator.Util.Direction;

public class MouseInput implements MouseListener, MouseMotionListener{
	
	private Zombinator z;
	
	public MouseInput(Zombinator z){this.z = z;}
	
	@Override
	public void mouseDragged(java.awt.event.MouseEvent arg0) {
		z.x = z.getMousePosition().x;
		z.y = z.getMousePosition().y;
	}

	@Override
	public void mouseMoved(java.awt.event.MouseEvent arg0) {
		z.x = z.getMousePosition().x;
		z.y = z.getMousePosition().y;
		
	}

	@Override
	public void mouseClicked(java.awt.event.MouseEvent arg0) {
		//TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(java.awt.event.MouseEvent arg0) {
		z.x = z.getMousePosition().x;
		z.y = z.getMousePosition().y;
	}

	@Override
	public void mouseExited(java.awt.event.MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(java.awt.event.MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent arg0) {
		Rectangle play = new Rectangle((z.WIDTH-256)/2, 400, 256, 64);
		Rectangle r1 = new Rectangle((z.WIDTH/2)+222, (12*48)+61, 48, 24);
		Point mouse = new Point(z.x, z.y);
		
		if(z.menu && !z.title){
			if(play.contains(mouse)){
				z.select = true;
				z.menu = false;
				Audio.playStoredSound("blip2", 0.5, 0);
			}
		}
		else if(z.select && !z.menu && !z.title){
			if(z.Primary){
				for(GunItem gi : GunItem.items){
					if(gi.getBounds().contains(mouse)){
						Zombinator.gunSel = gi.getGun().getGunId();
						Audio.playStoredSound("blip", 0.5, 0);
						z.primd = gi.getGun().getGunId();
						z.p.setPrimary(z.primd);
					}
				}
			}
			else{
				for(GunItem gi : GunItem.items){
					if(gi.getBounds().contains(mouse)){
						if(gi.getGun().isSecondary()){
							Zombinator.gunSel = gi.getGun().getGunId();
							Audio.playStoredSound("blip", 0.5, 0);
							z.sec = gi.getGun().getGunId();
							z.p.setSecondary(z.sec);
						}
					}
				}
			}
			
			if(r1.contains(mouse)){
				if(z.Primary){
					z.Primary = false;
					Zombinator.gunSel = -1;
					Audio.playStoredSound("blip2", 0.5, 0);
				}
				else{
					Zombinator.gunSel = -1;
					z.ingame = true;
					z.select = false;
					z.p = new Player((z.WIDTH/2), 600, Direction.LEFT, z.primd, z.sec);
					Audio.playStoredSound("blip2", 0.5, 0);
				}
			}
		}
	}

}
