package net.Archeantus.Zombinator.Guns;

import java.util.ArrayList;

import net.Archeantus.Zombinator.Entities.GunItem;

public class Gun{
	
	public static ArrayList<Gun> guns = new ArrayList<Gun>();
	
	private int rounds, bullets, fireRate, damage, knockback, reloadTime, clipSize, gunId;
	private String name;
	private boolean isSecondary;
	
	public void setRounds(int i){this.rounds = i;}
	public void setFireRate(int i){this.fireRate = i;}
	public void setDamage(int i){this.damage = i;}
	public void setKnockback(int i){this.knockback = i;}
	public void setReloadTime(int i){this.reloadTime = i;}
	public void setClipSize(int i){this.clipSize = i;}
	public void setBullets(int i){this.bullets = i;}
	
	public int getGunId(){return gunId;}
	public String getName(){return name;}
	public int getRounds(){return rounds;}
	public int getFireRate(){return fireRate;}
	public int getDamage(){return damage;}
	public int getKnockback(){return knockback;}
	public int getReloadTime(){return reloadTime;}
	public int getClipSize(){return clipSize;}
	public int getBullets(){return bullets;}
	public boolean isSecondary(){return isSecondary;}
	
	public Gun(int rounds, int clipSize, int dam, int fireRate, int knockback, int relTime, int id, String name, boolean b){
		this.setRounds(rounds);
		this.setClipSize(clipSize);
		this.setDamage(dam);
		this.setFireRate(fireRate);
		this.setKnockback(knockback);
		this.setReloadTime(relTime);
		this.setBullets(getClipSize());
		this.gunId = id;
		this.name = name;
		this.isSecondary = b;
		guns.add(this);
		new GunItem(this);
	}
}
