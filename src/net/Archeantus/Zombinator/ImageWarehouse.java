package net.Archeantus.Zombinator;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageWarehouse {
	
	private BufferedImage gunSheet1;
	private BufferedImage gunSheet2;
	private BufferedImage dpad;
	private BufferedImage entitySheet1;
	private BufferedImage gzombieSheet;
	private BufferedImage grassSheet;
	private BufferedImage healthSheet;
	private BufferedImage starSheet1;
	private BufferedImage starSheet2;
	private BufferedImage bloodSheet;
	private BufferedImage dustSheet;
	private BufferedImage bullet;
	private BufferedImage beam;
	private BufferedImage moon;
	private BufferedImage score;
	private BufferedImage ammo;
	private BufferedImage title;
	private BufferedImage logo;
	private BufferedImage buttonSheet;
	private BufferedImage background;
	private BufferedImage flagSheet;
	private BufferedImage chestSheet;
	private BufferedImage coinSheet;
	private BufferedImage orbSheet;
	private BufferedImage tileSheet;
	private BufferedImage guntileSheet;
	private BufferedImage buttontileSheet;
	private BufferedImage[] guns1;
	private BufferedImage[] guns2;
	private BufferedImage[] health;
	private BufferedImage[] stars1;
	private BufferedImage[] stars2;
	private BufferedImage[] zombieRight;
	private BufferedImage[] zombieLeft;
	private BufferedImage[] zrright;
	private BufferedImage[] zrleft;
	private BufferedImage[] playerRight;
	private BufferedImage[] playerLeft;
	private BufferedImage[] blood;
	private BufferedImage[] dust;
	private BufferedImage[] grass;
	private BufferedImage[] house;
	private BufferedImage[] buttons;
	private BufferedImage[] flag;
	private BufferedImage[] chest;
	private BufferedImage[] coin;
	private BufferedImage[] orb;
	private BufferedImage[] toptiles;
	private BufferedImage[] centertiles;
	private BufferedImage[] bottomtiles;
	private BufferedImage[] guntiles;
	private BufferedImage[] buttontiles1;
	private BufferedImage[] buttontiles2;
	private BufferedImage[] gzombieR;
	private BufferedImage[] gzombieL;
	
	public ImageWarehouse(Zombinator z){	
		try {
			gunSheet1 = ImageIO.read(z.getClass().getResource("resources/images/guns1.png"));
			gunSheet2 = ImageIO.read(z.getClass().getResource("resources/images/guns2.png"));
			dpad = ImageIO.read(z.getClass().getResource("resources/images/d1.png"));
			entitySheet1 = ImageIO.read(z.getClass().getResource("resources/images/entity1.png"));
			grassSheet = ImageIO.read(z.getClass().getResource("resources/images/grass.png"));
			healthSheet = ImageIO.read(z.getClass().getResource("resources/images/health.png"));
			starSheet1 = ImageIO.read(z.getClass().getResource("resources/images/stars.png"));
			starSheet2 = ImageIO.read(z.getClass().getResource("resources/images/stars2.png"));
			bullet = ImageIO.read(z.getClass().getResource("resources/images/bullet.png"));
			beam = ImageIO.read(z.getClass().getResource("resources/images/beam.png"));
			bloodSheet = ImageIO.read(z.getClass().getResource("resources/images/blood.png"));
			dustSheet = ImageIO.read(z.getClass().getResource("resources/images/dust.png"));
			moon = ImageIO.read(z.getClass().getResource("resources/images/moon.png"));
			score = ImageIO.read(z.getClass().getResource("resources/images/score.png"));
			ammo = ImageIO.read(z.getClass().getResource("resources/images/ammo.png"));
			title = ImageIO.read(z.getClass().getResource("resources/images/logo.png"));
			logo = ImageIO.read(z.getClass().getResource("resources/images/exanimated.png"));
			buttonSheet = ImageIO.read(z.getClass().getResource("resources/images/button.png"));
			background = ImageIO.read(z.getClass().getResource("resources/images/background.png"));
			flagSheet = ImageIO.read(z.getClass().getResource("resources/images/flag.png"));
			chestSheet = ImageIO.read(z.getClass().getResource("resources/images/chest.png"));
			coinSheet = ImageIO.read(z.getClass().getResource("resources/images/coin.png"));
			orbSheet = ImageIO.read(z.getClass().getResource("resources/images/exp.png"));
			tileSheet = ImageIO.read(z.getClass().getResource("resources/images/layout_tiles.png"));
			guntileSheet = ImageIO.read(z.getClass().getResource("resources/images/selection_tiles.png"));
			buttontileSheet = ImageIO.read(z.getClass().getResource("resources/images/button_tiles.png"));
			gzombieSheet = ImageIO.read(z.getClass().getResource("resources/images/gzombie.png"));
			
			guns1 = new BufferedImage[20];
			guns2 = new BufferedImage[20];
			health = new BufferedImage[4];
			stars1 = new BufferedImage[9];
			stars2 = new BufferedImage[9];
			zombieRight = new BufferedImage[4];
			zombieLeft = new BufferedImage[4];
			gzombieR = new BufferedImage[4];
			gzombieL = new BufferedImage[4];
			zrright = new BufferedImage[9];
			zrleft = new BufferedImage[9];
			blood = new BufferedImage[4];
			dust = new BufferedImage[4];
			playerRight = new BufferedImage[5];
			playerLeft = new BufferedImage[5];
			grass = new BufferedImage[4];
			house = new BufferedImage[3];
			buttons = new BufferedImage[2];
			flag = new BufferedImage[7];
			chest = new BufferedImage[6];
			coin = new BufferedImage[4];
			orb = new BufferedImage[4];
			toptiles = new BufferedImage[3];
			centertiles = new BufferedImage[3];
			bottomtiles = new BufferedImage[3];
			guntiles = new BufferedImage[3];
			buttontiles1 = new BufferedImage[2];
			buttontiles2 = new BufferedImage[2];
			
			house[0] = ImageIO.read(z.getClass().getResource("resources/images/house1.png"));
			house[1] = ImageIO.read(z.getClass().getResource("resources/images/house2.png"));
			house[2] = ImageIO.read(z.getClass().getResource("resources/images/barbedwirefence.png"));
			
			for(int i = 0; i < 5; i++){
				for(int j = 0; j < 4; j++){
					guns1[(i * 4) + j] = getImage(j, i, 16, 16, gunSheet1);
					guns2[(i * 4) + j] = getImage(j, i, 16, 16, gunSheet2);
				}
			}
			
			for(int i = 0; i < 2; i++){
				buttons[i] = getImage(i, 0, 128, 32, buttonSheet);
				buttontiles1[i] = getImage(i, 0, 16, 8, buttontileSheet);
				buttontiles2[i] = getImage(i, 1, 16, 8, buttontileSheet);
			}
			
			for(int i = 0; i < 3; i++){
				toptiles[i] = getImage(i, 0, 16, 16, tileSheet);
				centertiles[i] = getImage(i, 1, 16, 16, tileSheet);
				bottomtiles[i] = getImage(i, 2, 16, 16, tileSheet);
				guntiles[i] = getImage(i, 0, 20, 20, guntileSheet);
			}
			
			for(int i = 0; i < 4; i++){
				zombieRight[i] = getImage(i, 0, 20, 20, entitySheet1);
				zombieLeft[i] = getImage(i, 1, 20, 20, entitySheet1);
				gzombieR[i] = getImage(i, 0, 11, 26, gzombieSheet);
				gzombieL[i] = getImage(i, 1, 11, 26, gzombieSheet);
				blood[i] = getImage(i, 0, 8, 8, bloodSheet);
				dust[i] = getImage(i, 0, 8, 8, dustSheet);
				grass[i] = getImage(i, 0, 16, 16, grassSheet);
				health[i] = getImage(i, 0, 32, 32, healthSheet);
				coin[i] = getImage(i, 0, 16, 16, coinSheet);
				orb[i] = getImage(i, 0, 16, 16, orbSheet);
			}
			
			for(int i = 0; i < 5; i++){
				playerRight[i] = getImage(i, 4, 20, 20, entitySheet1);
				playerLeft[i] = getImage(i, 5, 20, 20, entitySheet1);
			}
			
			for(int i = 0; i < 9; i++){
				stars1[i] = getImage(i, 0, 20, 20, starSheet1);
				stars2[i] = getImage(i, 0, 20, 20, starSheet2);
				zrright[i] = getImage(i, 2, 20, 20, entitySheet1);
				zrleft[i] = getImage(i, 3, 20, 20, entitySheet1);
			}
			for(int i = 0; i < 7; i++){
				flag[i] = getImage(i, 0, 64, 64, flagSheet);
			}
			for(int i = 0; i < 6; i++){
				chest[i] = getImage(0, i, 54, 29, chestSheet);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public BufferedImage getGun(int i){return guns1[i];}
	public BufferedImage getGunFlipped(int i){return guns2[i];}
	public BufferedImage getPlayerPosRight(int i){return playerRight[i];}
	public BufferedImage getPlayerPosLeft(int i){return playerLeft[i];}
	public BufferedImage getZombiePosRight(int i){return zombieRight[i];}
	public BufferedImage getZombiePosLeft(int i){return zombieLeft[i];}
	public BufferedImage getGZombiePosRight(int i){return gzombieR[i];}
	public BufferedImage getGZombiePosLeft(int i){return gzombieL[i];}
	public BufferedImage getZombieRisingRight(int i){return zrright[i];}
	public BufferedImage getZombieRisingLeft(int i){return zrleft[i];}
	public BufferedImage getHealth(int i){return health[i];}
	public BufferedImage getStars1(int i){return stars1[i];}
	public BufferedImage getStars2(int i){return stars2[i];}
	public BufferedImage getBlood(int i){return blood[i];}
	public BufferedImage getDust(int i){return dust[i];}
	public BufferedImage getDpad(){return dpad;}
	public BufferedImage getGrass(int i){return grass[i];}
	public BufferedImage getBullet(){return bullet;}
	public BufferedImage getBeam(){return beam;}
	public BufferedImage getMoon(){return moon;}
	public BufferedImage getScore(){return score;}
	public BufferedImage getAmmo(){return ammo;}
	public BufferedImage getTitle(){return title;}
	public BufferedImage getLogo(){return logo;}
	public BufferedImage getCoin(int i){return coin[i];}
	public BufferedImage getOrb(int i){return orb[i];}
	public BufferedImage getBackground(){return background;}
	public BufferedImage getHouse(int i){return house[i];}
	public BufferedImage getButton(int i){return buttons[i];}
	public BufferedImage getFlag(int i){return flag[i];}
	public BufferedImage getChest(int i){return chest[i];}
	public BufferedImage getTopTile(int i){return toptiles[i];}
	public BufferedImage getCenterTile(int i){return centertiles[i];}
	public BufferedImage getBottomTile(int i){return bottomtiles[i];}
	public BufferedImage getGunTile(int i){return guntiles[i];}
	public BufferedImage getButtonTiles1(int i){return buttontiles1[i];}
	public BufferedImage getButtonTiles2(int i){return buttontiles2[i];}
	
	public BufferedImage getImage(int col, int row, int w, int h, BufferedImage sheet){
		return ((BufferedImage) sheet).getSubimage(col*w, row*h, w, h);
	}
}
