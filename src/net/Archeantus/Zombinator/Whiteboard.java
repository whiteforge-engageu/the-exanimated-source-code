package net.Archeantus.Zombinator;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import net.Archeantus.Zombinator.Entities.Ammo;
import net.Archeantus.Zombinator.Entities.BloodEffect;
import net.Archeantus.Zombinator.Entities.Bullet;
import net.Archeantus.Zombinator.Entities.DustEffect;
import net.Archeantus.Zombinator.Entities.GiantZombie;
import net.Archeantus.Zombinator.Entities.GunItem;
import net.Archeantus.Zombinator.Entities.Item;
import net.Archeantus.Zombinator.Entities.LaserBeam;
import net.Archeantus.Zombinator.Entities.Structure;
import net.Archeantus.Zombinator.Entities.Zombie;
import net.Archeantus.Zombinator.Entities.ZombieSpawnEffect;
import net.Archeantus.Zombinator.Util.Direction;
import net.Archeantus.Zombinator.Util.Logger;

public class Whiteboard {
	private Zombinator z;
	private int flagFrame = 0, maxFrames = 7, flagCount = 0, flagDelay = 5;
	
	public Whiteboard(Zombinator z){this.z = z;}
	
	public void drawMoon(){
		if(!z.im.getMoon().equals(null)){
			AffineTransform af = new AffineTransform();
			af.translate(150, 100);
			af.scale(4, 4);
			z.g2d.drawImage(z.im.getMoon(), af, z);
		}
	}
	
	public void drawDeathScreen(){
		Color c = new Color(237, 62, 62, 125);
		String waveTxt = "You made it to Wave "+z.waveNum;
		String msg = "You Died...";
		z.g2d.setColor(c);
		z.g2d.fill(z.screen);
		z.g2d.setFont(z.g2d.getFont().deriveFont(72F));
		z.g2d.setColor(Color.decode("#111111"));
		z.g2d.drawString(waveTxt, ((z.WIDTH-z.g.getStringWidth(waveTxt))/2)+4, 304);
		z.g2d.drawString(msg, ((z.WIDTH-z.g.getStringWidth(msg))/2)+4, 384);
		z.g2d.setColor(Color.RED);
		z.g2d.drawString(waveTxt, ((z.WIDTH-z.g.getStringWidth(waveTxt))/2), 300);
		z.g2d.setColor(Color.WHITE);
		z.g2d.drawString(msg, ((z.WIDTH-z.g.getStringWidth(msg))/2), 380);
	}
	
	public void drawMenu(){
		for(int i = 0; i < 12; i++){
			for(int j = 0; j < 12; j++){
				if(j == 0 && i == 0){
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getTopTile(0), af, z);
				}
				else if(j == 11 && i == 0){
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getTopTile(2), af, z);
				}
				else if(j == 0 && i == 11){
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getBottomTile(0), af, z);
				}
				else if(j == 11 && i == 11){
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getBottomTile(2), af, z);
				}
				else if(j == 0){
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getCenterTile(0), af, z);
				}
				else if(j == 11){
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getCenterTile(2), af, z);
				}
				else if(i == 0){
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getTopTile(1), af, z);
				}
				else if(i == 11){
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getBottomTile(1), af, z);
				}
				else{
					AffineTransform af = new AffineTransform();
					af.translate(((z.WIDTH/2)-((48*12)/2))+(j*48), (i*48)+97);
					af.scale(3, 3);
					z.g2d.drawImage(z.im.getCenterTile(1), af, z);
				}
			}
		}
		
		Rectangle r1 = new Rectangle((z.WIDTH/2)+222, (12*48)+61, 48, 24);
		Point mouse = new Point(z.x, z.y);
		
		if(z.Primary){
			z.g2d.setColor(Color.BLACK);
			z.g2d.setFont(z.g2d.getFont().deriveFont(28F));
			z.g2d.drawString("Choose Primary Weapon", (z.WIDTH/2)-(z.g.getStringWidth("Choose Primary Weapon")/2)+2, 162);
			z.g2d.setColor(Color.WHITE);
			z.g2d.drawString("Choose Primary Weapon", (z.WIDTH/2)-(z.g.getStringWidth("Choose Primary Weapon")/2), 160);
			if(r1.contains(mouse)){
				AffineTransform af = new AffineTransform();
				af.translate((z.WIDTH/2)+222, (12*48)+61);
				af.scale(3, 3);
				z.g2d.drawImage(z.im.getButtonTiles2(1), af, z);
			}
			else{
				AffineTransform af = new AffineTransform();
				af.translate((z.WIDTH/2)+222, (12*48)+61);
				af.scale(3, 3);
				z.g2d.drawImage(z.im.getButtonTiles2(0), af, z);
			}
		}
		else{
			z.g2d.setColor(Color.BLACK);
			z.g2d.setFont(z.g2d.getFont().deriveFont(28F));
			z.g2d.drawString("Choose Secondary Weapon", (z.WIDTH/2)-(z.g.getStringWidth("Choose Secondary Weapon")/2)+2, 162);
			z.g2d.setColor(Color.WHITE);
			z.g2d.drawString("Choose Secondary Weapon", (z.WIDTH/2)-(z.g.getStringWidth("Choose Secondary Weapon")/2), 160);
			if(r1.contains(mouse)){
				AffineTransform af = new AffineTransform();
				af.translate((z.WIDTH/2)+222, (12*48)+61);
				af.scale(3, 3);
				z.g2d.drawImage(z.im.getButtonTiles1(1), af, z);
			}
			else{
				AffineTransform af = new AffineTransform();
				af.translate((z.WIDTH/2)+222, (12*48)+61);
				af.scale(3, 3);
				z.g2d.drawImage(z.im.getButtonTiles1(0), af, z);
			}
		}
		
		drawGuns();
	}
	
	public void drawGuns(){
		Point p = new Point(z.x, z.y);
		
		if(z.Primary){
			for(int i = 0; i < GunItem.items.size(); i++){
				GunItem g = GunItem.items.get(i);
				if(!g.getGun().isSecondary()){
					if(!g.getBounds().contains(p)){
						AffineTransform af = new AffineTransform();
						af.translate(g.getX(), g.getY());
						af.scale(3, 3);
						z.g2d.drawImage(z.im.getGunTile(0), af, z);
						AffineTransform af1 = new AffineTransform();
						af1.translate(g.getX()+7, g.getY()+7);
						af1.scale(3, 3);
						z.g2d.drawImage(z.im.getGun(g.getGun().getGunId()), af1, z);
					}
					else{
						AffineTransform af = new AffineTransform();
						af.translate(g.getX(), g.getY());
						af.scale(3, 3);
						z.g2d.drawImage(z.im.getGunTile(1), af, z);
						AffineTransform af1 = new AffineTransform();
						af1.translate(g.getX()+7, g.getY()+7);
						af1.scale(3, 3);
						z.g2d.drawImage(z.im.getGun(g.getGun().getGunId()), af1, z);
						
					}
				}
			}
		}
		else{
			for(int i = 0; i < GunItem.items.size(); i++){
				GunItem g = GunItem.items.get(i);
				if(g.getGun().isSecondary()){
					if(!g.getBounds().contains(p)){
						AffineTransform af = new AffineTransform();
						af.translate(g.getX(), g.getY());
						af.scale(3, 3);
						z.g2d.drawImage(z.im.getGunTile(0), af, z);
						AffineTransform af1 = new AffineTransform();
						af1.translate(g.getX()+10, g.getY()+5);
						af1.scale(3, 3);
						z.g2d.drawImage(z.im.getGun(g.getGun().getGunId()), af1, z);
					}
					else{
						AffineTransform af = new AffineTransform();
						af.translate(g.getX(), g.getY());
						af.scale(3, 3);
						z.g2d.drawImage(z.im.getGunTile(1), af, z);
						AffineTransform af1 = new AffineTransform();
						af1.translate(g.getX()+10, g.getY()+5);
						af1.scale(3, 3);
						z.g2d.drawImage(z.im.getGun(g.getGun().getGunId()), af1, z);
					}
				}
			}
		}
		
		for(GunItem gi : GunItem.items){
			if(Zombinator.gunSel == gi.getGun().getGunId()){
				AffineTransform af1 = new AffineTransform();
				af1.translate(gi.getX(), gi.getY());
				af1.scale(3, 3);
				z.g2d.drawImage(z.im.getGunTile(2), af1, z);
			}
		}
		
		drawMouseOvers(p);
		
	}
	
	public void drawMouseOvers(Point p){
		if(z.Primary){
			for(GunItem gi : GunItem.items){
				if(gi.getBounds().contains(p)){
					if(!gi.getGun().isSecondary()){
						String[] lines = {gi.getGun().getName(),"Rounds: "+gi.getGun().getRounds(),"Bullets per clip: "+gi.getGun().getClipSize(),"Damage per bullet: "+gi.getGun().getDamage()+" HP"};
						int maxW = 230;
						int maxH = (18*lines.length)+10;
						int x = z.x+48;
						int y = z.y;
						Color color = new Color(0, 0, 0, 196);
						z.g2d.setColor(color);
						z.g2d.fillRect(x, y, maxW+2, maxH+2);
						z.g2d.setColor(Color.decode("#9E0000"));
						z.g2d.draw(new Rectangle(x-1, y-1, maxW+2, maxH+2));
						z.g2d.draw(new Rectangle(x-2, y-2, maxW+4, maxH+4));
						z.g2d.setColor(Color.decode("#666666"));
						z.g2d.draw(new Rectangle(x-3, y-3, maxW+6, maxH+6));
						z.g2d.draw(new Rectangle(x-4, y-4, maxW+8, maxH+8));
						
						x += 10;
						y += 20;
						
						z.g2d.setFont(z.g2d.getFont().deriveFont(14F));
						z.g2d.setColor(Color.WHITE);
						for(int i = 0; i < lines.length; i++){
							z.g2d.drawString(lines[i], x, y+(i*18)+10);
						}
						z.g2d.setColor(Color.WHITE);
					}
				}
			}
		}
		else{
			for(GunItem gi : GunItem.items){
				if(gi.getBounds().contains(p)){
					if(gi.getGun().isSecondary()){
						String[] lines = {gi.getGun().getName(),"Rounds: "+gi.getGun().getRounds(),"Bullets per clip: "+gi.getGun().getClipSize(),"Damage per bullet: "+gi.getGun().getDamage()+" HP"};
						int maxW = 230;
						int maxH = (18*lines.length)+10;
						int x = z.x+48;
						int y = z.y;
						Color color = new Color(0, 0, 0, 196);
						z.g2d.setColor(color);
						z.g2d.fillRect(x, y, maxW+2, maxH+2);
						z.g2d.setColor(Color.decode("#9E0000"));
						z.g2d.draw(new Rectangle(x-1, y-1, maxW+2, maxH+2));
						z.g2d.draw(new Rectangle(x-2, y-2, maxW+4, maxH+4));
						z.g2d.setColor(Color.decode("#666666"));
						z.g2d.draw(new Rectangle(x-3, y-3, maxW+6, maxH+6));
						z.g2d.draw(new Rectangle(x-4, y-4, maxW+8, maxH+8));
						
						x += 10;
						y += 20;
						
						z.g2d.setFont(z.g2d.getFont().deriveFont(14F));
						z.g2d.setColor(Color.WHITE);
						for(int i = 0; i < lines.length; i++){
							z.g2d.drawString(lines[i], x, y+(i*18)+10);
						}
						z.g2d.setColor(Color.WHITE);
					}
				}
			}
		}
	}
	
	public void drawTime(){
		String s = (new StringBuilder()).append(z.clock.getClockMinutes()).append(":").append(z.clock.getClockSeconds()).toString();
		z.g2d.setFont(z.g2d.getFont().deriveFont(24F));
		z.g2d.setColor(Color.WHITE);
		int y1 = 80;
		if(!z.nheader){y1 = 55;}
		z.g2d.drawString(s, ((z.WIDTH-z.g.getStringWidth(s))/2), y1);
		
		if(z.clock.wavedone){
			String st = (new StringBuilder()).append("00:").append(z.clock.getTimeLeft()).toString();
			z.g2d.setFont(z.g2d.getFont().deriveFont(36F));
			z.g2d.setColor(Color.WHITE);
			int y = 280;
			if(!z.nheader){y = 255;}
			z.g2d.drawString("Next wave in:", ((z.WIDTH-z.g.getStringWidth("Next wave in:"))/2), y);
			z.g2d.setFont(z.g2d.getFont().deriveFont(72F));
			z.g2d.setColor(Color.decode("#111111"));
			z.g2d.drawString(st, ((z.WIDTH-z.g.getStringWidth(st))/2)+4, 104+y);
			z.g2d.setColor(Color.red);
			z.g2d.drawString(st, ((z.WIDTH-z.g.getStringWidth(st))/2), 100+y);
		}
	}
	
	public void drawWaveText(){
		String s = (new StringBuilder()).append("Wave").append(" ").append(z.waveNum).toString();
		z.g2d.setFont(z.g2d.getFont().deriveFont(72F));
		int y = 380;
		if(!z.nheader){y = 355;}
		if(z.timeNextShow == 0){
			if((z.colvar > 0) && z.wavetext){
				z.g2d.setColor(new Color(255, 255, 255, z.colvar));
				z.g2d.drawString(s, ((z.WIDTH-z.g.getStringWidth(s))/2)-4, y);
				z.colvar -= 5;
			}
			else{
				z.wavetext = false;
				z.timeNextShow = 100;
			}
		}
		else{
			z.timeNextShow--;
			z.g2d.setColor(new Color(255, 255, 255, z.colvar));
			z.g2d.drawString(s, ((z.WIDTH-z.g.getStringWidth(s))/2)-4, y);
		}
	}
	
	public void drawEntities(){
		for(Ammo a : Ammo.ammoEntities){
			if(a.EntityIsAlive()){
				if(z.screen.intersects(a.getBounds(z.offset))){
					//z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(a.getBounds(z.offset));
					AffineTransform af = new AffineTransform();
					af.translate(a.getX()-z.offset, a.getY()-32);
					af.scale(2, 2);
					z.g2d.drawImage(z.im.getAmmo(), af, z);
				}
				a.updateEntity();
			}
		}
		
		for(Item i : Item.itemEntities){
			if(i.EntityIsAlive()){
				if(z.screen.intersects(i.getBounds(z.offset))){
					if(i.getType() == 1){
						//z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(i.getBounds(z.offset));
						AffineTransform af = new AffineTransform();
						af.translate(i.getX()-z.offset, i.getY()-32);
						af.scale(1, 1);
						z.g2d.drawImage(z.im.getCoin(i.getFrame()), af, z);
						i.updateEntity();
					}
					else if(i.getType() == 2){
						//z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(i.getBounds(z.offset));
						AffineTransform af = new AffineTransform();
						af.translate(i.getX()-z.offset, i.getY()-32);
						af.scale(1, 1);
						z.g2d.drawImage(z.im.getOrb(i.getFrame()), af, z);
						i.updateEntity();
					}
				}
			}
		}
	}
	
	public void drawGrass(){
		if(!z.im.getGrass(0).equals(null)){
			for(int i = 0; i < 58; i++){
				Rectangle r = new Rectangle(((128*i)-z.offset)-640, 600, 128, 128);
				if(z.screen.intersects(r)){
					AffineTransform af = new AffineTransform();
					af.translate(((128*i)-z.offset)-640, 600);
					af.scale(8, 8);
					z.g2d.drawImage(z.im.getGrass(z.g.getGrassArray()[i]), af, z);
				}
			}
		}
	}
	
	public void drawStructures(){
		for(Structure s : Structure.structures){
			if(z.screen.intersects(s.getBounds(z.offset))){
				//z.g2d.setColor(Color.WHITE);
				//z.g2d.draw(s.getBounds(z.offset));
				z.g2d.drawImage(z.im.getHouse(s.getType()), s.getX()-z.offset, (600+s.getY())-s.getHeight(), z);
			}
		}
	}
	
	public void drawChest(){
		if(z.screen.intersects(z.c.getBounds(z.offset))){
			AffineTransform af = new AffineTransform();
			af.translate(z.c.getX()-z.offset, (600+z.c.getY())-58);
			af.scale(2, 2);
			z.g2d.drawImage(z.im.getChest(z.c.getFrame()), af, z);
			z.c.updateFrame();
			
			if(!z.c.isOpen()){
				Rectangle r = new Rectangle((z.c.getX()-z.offset)+4, z.c.getY()+500, 100, 40);
				z.g2d.setColor(new Color(0, 0, 0, 125));
				z.g2d.fill(r);
				z.g2d.setColor(Color.WHITE);
				z.g2d.setFont(z.g2d.getFont().deriveFont(10F));
				z.g2d.drawImage(z.im.getCoin(0), ((((108/2)-(16/2))+z.c.getX())-z.offset)-15, z.c.getY()+503, z);
				z.g2d.drawString(z.price+"", ((((108/2)-(z.g.getStringWidth(z.coins+"")/2))+z.c.getX())-z.offset)+15, z.c.getY()+521);
				z.g2d.drawString("To Open", (((108/2)-(z.g.getStringWidth("To Open")/2))+z.c.getX())-z.offset, z.c.getY()+540);
			}
		}
	}
	
	public void drawFPS(){
		z.g2d.setColor(Color.WHITE);
		z.g2d.setFont(z.g2d.getFont().deriveFont(12F));
		
		int y = 60;
		if(!z.nheader){y = 35;}
		
		z.g2d.drawString("FPS  "+(z.frameRate-1), 30, y);
		z.g2d.drawString(z.version, 30, y+30);
		z.g2d.drawString("Coords: "+(z.offset/10)+", "+((z.p.getY()-600)*-1)/10, 30, y+60);
		
		int x = (z.WIDTH-z.g.getStringWidth(z.coins+""))-44;
		z.g2d.drawImage(z.im.getCoin(0), x, y+40, z);
		z.g2d.setColor(Color.WHITE);
		z.g2d.setFont(z.g2d.getFont().deriveFont(14F));
		z.g2d.drawString(z.coins+"", x+26, y+62);
		x = (z.WIDTH-z.g.getStringWidth(z.exp+""))-42;
		z.g2d.drawImage(z.im.getOrb(0), x, y+70, z);
		z.g2d.drawString(z.exp+"", x+26, y+92);
	}
	
	public void drawScore(){
		AffineTransform af = new AffineTransform();
		int y1 = 25;
		if(!z.nheader){y1 = 0;}
		af.translate(((z.WIDTH-(192*2))/2), y1);
		af.scale(2, 2);
		int y = 160;
		if(!z.nheader){y = 135;}
		z.g2d.drawImage(z.im.getScore(), af, z);
		z.g2d.setFont(z.g2d.getFont().deriveFont(36F));
		z.g2d.setColor(Color.decode("#111111"));
		z.g2d.drawString(""+z.score, ((z.WIDTH-z.g.getStringWidth(""+z.score))/2)+2, y+2);
		z.g2d.setColor(Color.decode("#CCCCCC"));
		z.g2d.drawString(""+z.score, ((z.WIDTH-z.g.getStringWidth(""+z.score))/2), y);
	}
	
	public void drawDpad(){
		z.g2d.drawImage(z.im.getDpad(), 20, (z.HEIGHT - 220), z);
	}
	
	public void drawStars(){
		int[][] stars = z.g.getStarArray();
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 16; j++){
				AffineTransform t = new AffineTransform();
				t.translate(j*80, i*80);
				t.scale(4, 4);
				if(!z.eclipse){z.g2d.drawImage(z.im.getStars1(stars[i][j]), t, z);}else{z.g2d.drawImage(z.im.getStars2(stars[i][j]), t, z);}
			}
		}
		
		Color blend = new Color(236, 10, 47, 0);
		Color blend2 = new Color(5, 2, 94, 0);
		
		if(z.eclipse){
			GradientPaint paint = new GradientPaint(0, 0, Color.BLACK, 0, 300, blend);
	        z.g2d.setPaint(paint);
	        z.g2d.fillRect(0, 0, 1280, 300);
	        paint = new GradientPaint(0, 300, blend, 0, 600, Color.decode("#fa5f2f"));
	        z.g2d.setPaint(paint);
	        z.g2d.fillRect(0, 300, 1280, 300);
		}
		else{
			GradientPaint paint = new GradientPaint(0, 0, Color.BLACK, 0, 300, blend2);
	        z.g2d.setPaint(paint);
	        z.g2d.fillRect(0, 0, 1280, 300);
	        paint = new GradientPaint(0, 300, blend2, 0, 600, Color.decode("#6e26b8"));
	        z.g2d.setPaint(paint);
	        z.g2d.fillRect(0, 300, 1280, 300);
		}
	}
	
	public void drawMessages(){
		for(int i = 0;i < Logger.messages.size(); i++){
			//z.g2d.setColor(Color.BLACK);
			//z.g2d.fillRect(10, 16-(16*i)+200, z.g.getStringWidth(Logger.messages.get(i)), 16);
			z.g2d.setFont(z.g2d.getFont().deriveFont(12F));
			z.g2d.setColor(Color.decode("#fefefe"));
			z.g2d.drawString(Logger.messages.get(i), 18, ((16-(16*i))-2)+200);
		}
	}
	
	public void drawBackground(){
		AffineTransform af = new AffineTransform();
		af.translate(0, 0);
		af.scale(8, 8);
		z.g2d.drawImage(z.im.getBackground(), af, z);
		
		AffineTransform ag = new AffineTransform();
		ag.translate(96, 145);
		ag.scale(4, 4);
		z.g2d.drawImage(z.im.getFlag(flagFrame), ag, z);
		
		flagCount++;
		if(flagCount > flagDelay){flagCount = 0;flagFrame++; if(flagFrame >= maxFrames){flagFrame = 0;}}
	}
	
	public void drawMenuButtons(){
		Rectangle play = new Rectangle((z.WIDTH-256)/2, 400, 256, 64);
		Point mouse = new Point(z.x, z.y);
		
		if(play.contains(mouse)){
			AffineTransform af = new AffineTransform();
			af.translate((z.WIDTH-256)/2, 400);
			af.scale(2, 2);
			z.g2d.drawImage(z.im.getButton(1), af, z);
			z.g2d.setColor(Color.decode("#111111"));
			z.g2d.setFont(z.g2d.getFont().deriveFont(24F));
			z.g2d.drawString("Play", ((z.WIDTH-z.g.getStringWidth("Play"))/2)+2, 456);
			z.g2d.setColor(Color.decode("#ff9f00"));
			z.g2d.drawString("Play", (z.WIDTH-z.g.getStringWidth("Play"))/2, 454);
		}
		else{
			AffineTransform af = new AffineTransform();
			af.translate((z.WIDTH-256)/2, 400);
			af.scale(2, 2);
			z.g2d.drawImage(z.im.getButton(0), af, z);
			z.g2d.setColor(Color.decode("#111111"));
			z.g2d.setFont(z.g2d.getFont().deriveFont(24F));
			z.g2d.drawString("Play", ((z.WIDTH-z.g.getStringWidth("Play"))/2)+2, 456);
			z.g2d.setColor(Color.WHITE);
			z.g2d.drawString("Play", (z.WIDTH-z.g.getStringWidth("Play"))/2, 454);
		}
		
	}
	
	public void drawGun(){
		AffineTransform t = new AffineTransform();
		t.translate(30, 510);
		t.scale(10, 10);
		z.g2d.drawImage(z.im.getGunFlipped(z.p.getCurrent()), t, z);
		z.g2d.setColor(Color.WHITE);
		z.g2d.setFont(z.g2d.getFont().deriveFont(18F));
		z.g2d.drawString(z.g.getName(z.p.getCurrent()), ((200-z.g.getStringWidth(z.g.getName(z.p.getCurrent())))/2)+20, (z.HEIGHT-260));
		if(!z.reload){z.g2d.drawString(z.g.getBullets(z.p.getCurrent())+" - "+z.g.getRounds(z.p.getCurrent()), ((200-z.g.getStringWidth(z.g.getBullets(z.p.getCurrent())+" - "+z.g.getRounds(z.p.getCurrent())))/2)+20, (z.HEIGHT-240));}else{z.g2d.drawString("Reloading", ((200-z.g.getStringWidth("Reloading"))/2)+20, (z.HEIGHT-240));}
		
	}
	
	public void drawHealth(){
		int i = z.p.getMaxHealth();
		int s = z.p.getHealth();
		i = i/2;
		int size = 32*i;
		int y = 50;
		if(!z.nheader){y = 25;}
		
		for(int j = 0; j < i; j++){
			z.g2d.drawImage(z.im.getHealth(2), (z.WIDTH-(size+30))+(32*j), y, z);
		}
		
		
		int numberOfFilledHearts = s / 2;
		boolean extraHalfHeart = s % 2 == 1;
		
		int i1 = 0;
		for (; i1 < numberOfFilledHearts; i1++) {
			z.g2d.drawImage(z.im.getHealth(0), (z.WIDTH-(size+30))+(32*i1), y, z);
		}
		if (extraHalfHeart){
			z.g2d.drawImage(z.im.getHealth(1), (z.WIDTH-(size+30))+(32*i1), y, z);
		}
		
		if(z.p.flashed()){
			for(int j = 0; j < i; j++){
				z.g2d.drawImage(z.im.getHealth(3), (z.WIDTH-(size+30))+(32*j), y, z);
			}
		}
	}
	
	public void renderGun(){
		if(z.p.isAlive()){
			AffineTransform t = new AffineTransform();
			if(z.p.getDirection() == Direction.LEFT){
				t.translate(z.p.getX()-50, z.p.getY()-45);
				t.scale(3, 3);
				t.rotate(Math.toRadians(-44));
				z.g2d.drawImage(z.im.getGun(z.p.getCurrent()), t, z);
			}
			else if(z.p.getDirection() == Direction.RIGHT){
				t.translate(z.p.getX()+26, z.p.getY()-78);
				t.scale(3, 3);
				t.rotate(Math.toRadians(44));
				z.g2d.drawImage(z.im.getGunFlipped(z.p.getCurrent()), t, z);
			}
		}
	}
	
	public void drawBlood(){
		for(BloodEffect b : BloodEffect.bloodSpills){
			if(b.EntityIsAlive()){
				AffineTransform af = new AffineTransform();
				af.translate(b.getX()-z.offset, b.getY());
				af.scale(4, 4);
				z.g2d.drawImage(z.im.getBlood(b.getFrame()), af, z);
				b.updateFrame();
			}
		}
	}
	
	public void drawDust(){
		for(DustEffect b : DustEffect.dust){
			if(b.EntityIsAlive()){
				AffineTransform af = new AffineTransform();
				af.translate(b.getX()-z.offset, b.getY());
				af.scale(4, 4);
				z.g2d.drawImage(z.im.getDust(b.getFrame()), af, z);
				b.updateFrame();
			}
		}
	}
	
	public void drawBullets(){
		Rectangle abounds = new Rectangle(z.offset*(-1), 0, z.mapSize, z.HEIGHT);
		for(Bullet b : Bullet.bullets){
			if(b.EntityIsAlive()){
				if(b.getDirection() == Direction.LEFT){
					if(z.screen.intersects(b.getBounds())){z.g2d.drawImage(z.im.getBullet(), b.getX(), b.getY(), z);}
					b.setX(b.getX()-b.getVelocity());
					if(!abounds.contains(b.getBounds())){
						b.setAlive(false);
					}
				}
				else if(b.getDirection() == Direction.RIGHT){
					AffineTransform af = new AffineTransform();
					af.translate(b.getX(), b.getY());
					af.rotate(Math.toRadians(180));
					if(z.screen.intersects(b.getBounds())){z.g2d.drawImage(z.im.getBullet(), af, z);}
					b.setX(b.getX()+b.getVelocity());
					if(!abounds.contains(b.getBounds())){
						b.setAlive(false);
					}
				}
			}
		}
		
		for(LaserBeam b : LaserBeam.bullets){
			if(b.EntityIsAlive()){
				if(b.getDirection() == Direction.LEFT){
					if(z.screen.intersects(b.getBounds())){z.g2d.drawImage(z.im.getBeam(), b.getX(), b.getY(), z);}
					b.setX(b.getX()-b.getVelocity());
					if(!abounds.contains(b.getBounds())){
						b.setAlive(false);
					}
				}
				else if(b.getDirection() == Direction.RIGHT){
					AffineTransform af = new AffineTransform();
					af.translate(b.getX(), b.getY());
					af.rotate(Math.toRadians(180));
					if(z.screen.intersects(b.getBounds())){z.g2d.drawImage(z.im.getBeam(), af, z);}
					b.setX(b.getX()+b.getVelocity());
					if(!abounds.contains(b.getBounds())){
						b.setAlive(false);
					}
				}
			}
		}
	}
	
	public void drawHealthBar(Zombie zo){
		int max = zo.getMaxHealth();
		int health = zo.getHealth();
		int dhealth = (health*6);
		
		if(dhealth != (max*6)){
			if(z.screen.intersects(zo.getBounds())){
				Rectangle m = new Rectangle((zo.getX()-30)-z.offset, zo.getY()-90, 60, 5);
				Rectangle m1 = new Rectangle((zo.getX()-30)-z.offset, zo.getY()-90, dhealth, 6);
				z.g2d.setColor(Color.RED);
				z.g2d.draw(m);
				z.g2d.setColor(Color.GREEN);
				z.g2d.fill(m1);
			}
		}
	}
	
	public void drawHealthBarG(GiantZombie zo){
		int max = zo.getMaxHealth();
		int health = zo.getHealth();
		int dhealth = (health*6);
		
		if(dhealth != (max*6)){
			if(z.screen.intersects(zo.getBounds())){
				Rectangle m = new Rectangle((zo.getX()-82)-z.offset, zo.getY()-114, 120, 5);
				Rectangle m1 = new Rectangle((zo.getX()-82)-z.offset, zo.getY()-114, dhealth, 6);
				z.g2d.setColor(Color.RED);
				z.g2d.draw(m);
				z.g2d.setColor(Color.GREEN);
				z.g2d.fill(m1);
			}
		}
	}
	
	@SuppressWarnings("static-access")
	public void drawNametag(){
		z.g2d.setFont(z.g2d.getFont().deriveFont(10F));
		int sizex = z.g.getStringWidth(z.player)+8;
		int sizey = 18;
		int x = (z.WIDTH/2)-(sizex/2);
		Rectangle m = new Rectangle(x, z.p.getY()-100, sizex, sizey);
		z.g2d.setColor(new Color(0, 0, 0, 125));
		z.g2d.fill(m);
		z.g2d.setColor(Color.WHITE);
		z.g2d.drawString(z.player, x+4, z.p.getY()-81);
		
	}
	
	public void drawZombies(){
		for(Zombie zo : Zombie.zombies){
			if(zo.isAlive()){
				if(zo.isMoving()){
					if(zo.getDirection() == Direction.LEFT){
						z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(zo.getBounds());
						AffineTransform t = new AffineTransform();
						t.translate((zo.getX()-20)-z.offset, zo.getY()-80);
						t.scale(4, 4);
						if(z.screen.intersects(zo.getBounds())){z.g2d.drawImage(z.im.getZombiePosLeft(zo.getFrame()), t, z);}
						drawHealthBar(zo);
						zo.updateFrame();
						zo.setX(zo.getX()-zo.getSpeed());
						zo.setBox(zo.getX()-z.offset);
					}
					else if(zo.getDirection() == Direction.RIGHT){
						z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(zo.getBounds());
						AffineTransform t = new AffineTransform();
						t.translate((zo.getX()-20)-z.offset, zo.getY()-80);
						t.scale(4, 4);
						if(z.screen.intersects(zo.getBounds())){z.g2d.drawImage(z.im.getZombiePosRight(zo.getFrame()), t, z);}
						drawHealthBar(zo);
						zo.updateFrame();
						zo.setX(zo.getX()+zo.getSpeed());
						zo.setBox(zo.getX()-z.offset);
					}
				}
				else{
					if(zo.getDirection() == Direction.LEFT){
						z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(zo.getBounds());
						AffineTransform t = new AffineTransform();
						t.translate((zo.getX()-20)-z.offset, zo.getY()-80);
						t.scale(4, 4);
						if(z.screen.intersects(zo.getBounds())){z.g2d.drawImage(z.im.getZombiePosLeft(0), t, z);}
						drawHealthBar(zo);
					}
					else if(zo.getDirection() == Direction.RIGHT){
						z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(zo.getBounds());
						AffineTransform t = new AffineTransform();
						t.translate((zo.getX()-20)-z.offset, zo.getY()-80);
						t.scale(4, 4);
						if(z.screen.intersects(zo.getBounds())){z.g2d.drawImage(z.im.getZombiePosRight(0), t, z);}
						drawHealthBar(zo);
					}
				}
			}
		}
		
		for(GiantZombie zo : GiantZombie.zombies){
			if(zo.isAlive()){
				if(zo.isMoving()){
					if(zo.getDirection() == Direction.LEFT){
						z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(zo.getBounds());
						AffineTransform t = new AffineTransform();
						t.translate((zo.getX()-44)-z.offset, zo.getY()-104);
						t.scale(4, 4);
						if(z.screen.intersects(zo.getBounds())){z.g2d.drawImage(z.im.getGZombiePosLeft(zo.getFrame()), t, z);}
						drawHealthBarG(zo);
						zo.updateFrame();
						zo.setX(zo.getX()-zo.getSpeed());
						zo.setBox(zo.getX()-z.offset);
					}
					else if(zo.getDirection() == Direction.RIGHT){
						z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(zo.getBounds());
						AffineTransform t = new AffineTransform();
						t.translate((zo.getX()-44)-z.offset, zo.getY()-104);
						t.scale(4, 4);
						if(z.screen.intersects(zo.getBounds())){z.g2d.drawImage(z.im.getGZombiePosRight(zo.getFrame()), t, z);}
						drawHealthBarG(zo);
						zo.updateFrame();
						zo.setX(zo.getX()+zo.getSpeed());
						zo.setBox(zo.getX()-z.offset);
					}
				}
				else{
					if(zo.getDirection() == Direction.LEFT){
						z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(zo.getBounds());
						AffineTransform t = new AffineTransform();
						t.translate((zo.getX()-44)-z.offset, zo.getY()-104);
						t.scale(4, 4);
						if(z.screen.intersects(zo.getBounds())){z.g2d.drawImage(z.im.getGZombiePosLeft(0), t, z);}
						drawHealthBarG(zo);
					}
					else if(zo.getDirection() == Direction.RIGHT){
						z.g2d.setColor(Color.WHITE);
						//z.g2d.draw(zo.getBounds());
						AffineTransform t = new AffineTransform();
						t.translate((zo.getX()-44)-z.offset, zo.getY()-104);
						t.scale(4, 4);
						if(z.screen.intersects(zo.getBounds())){z.g2d.drawImage(z.im.getGZombiePosRight(0), t, z);}
						drawHealthBarG(zo);
					}
				}
			}
		}
		
		for(ZombieSpawnEffect zi : ZombieSpawnEffect.getSpawning){
			if(zi.EntityIsAlive()){
				if(zi.getDirection() == Direction.LEFT){
					AffineTransform af = new AffineTransform();
					af.translate((zi.getX()-20)-z.offset, zi.getY()-80);
					af.scale(4, 4);
					z.g2d.drawImage(z.im.getZombieRisingLeft(zi.getFrame()), af, z);
					zi.updateFrame();
				}
				else if(zi.getDirection() == Direction.RIGHT){
					AffineTransform af = new AffineTransform();
					af.translate((zi.getX()-36)-z.offset, zi.getY()-80);
					af.scale(4, 4);
					z.g2d.drawImage(z.im.getZombieRisingRight(zi.getFrame()), af, z);
					zi.updateFrame();
				}
			}
		}
	}
	
	public void drawPlayer(){
		if(z.p.isAlive()){
			if(z.p.isJumping() && z.p.isMoving()){
				if(z.p.getDirection() == Direction.RIGHT){
					z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(z.p.getBounds());
					AffineTransform t = new AffineTransform();
					t.translate(z.p.getX()-20, z.p.getY()-80);
					t.scale(4, 4);
					z.g2d.drawImage(z.im.getPlayerPosRight(4), t, z);
					drawNametag();
				}
				else if(z.p.getDirection() == Direction.LEFT){
					z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(z.p.getBounds());
					AffineTransform t = new AffineTransform();
					t.translate(z.p.getX()-20, z.p.getY()-80);
					t.scale(4, 4);
					z.g2d.drawImage(z.im.getPlayerPosLeft(4), t, z);
					drawNametag();
				}
			}
			else if(z.p.isJumping() && !z.p.isMoving()){
				if(z.p.getDirection() == Direction.RIGHT){
					z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(z.p.getBounds());
					AffineTransform t = new AffineTransform();
					t.translate(z.p.getX()-20, z.p.getY()-80);
					t.scale(4, 4);
					z.g2d.drawImage(z.im.getPlayerPosRight(4), t, z);
					drawNametag();
				}
				else if(z.p.getDirection() == Direction.LEFT){
					z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(z.p.getBounds());
					AffineTransform t = new AffineTransform();
					t.translate(z.p.getX()-20, z.p.getY()-80);
					t.scale(4, 4);
					z.g2d.drawImage(z.im.getPlayerPosLeft(4), t, z);
					drawNametag();
				}
			}
			else if(z.p.isMoving() && !z.p.isJumping()){
				if(z.p.getDirection() == Direction.RIGHT){
					z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(z.p.getBounds());
					AffineTransform t = new AffineTransform();
					t.translate(z.p.getX()-20, z.p.getY()-80);
					t.scale(4, 4);
					z.g2d.drawImage(z.im.getPlayerPosRight(z.p.getFrame()), t, z);
					z.p.updateFrame();
					drawNametag();
				}
				else if(z.p.getDirection() == Direction.LEFT){
					z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(z.p.getBounds());
					AffineTransform t = new AffineTransform();
					t.translate(z.p.getX()-20, z.p.getY()-80);
					t.scale(4, 4);
					z.g2d.drawImage(z.im.getPlayerPosLeft(z.p.getFrame()), t, z);
					z.p.updateFrame();
					drawNametag();
				}
			}
			else{
				if(z.p.getDirection() == Direction.RIGHT){
					z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(z.p.getBounds());
					AffineTransform t = new AffineTransform();
					t.translate(z.p.getX()-20, z.p.getY()-80);
					t.scale(4, 4);
					z.g2d.drawImage(z.im.getPlayerPosRight(0), t, z);
					drawNametag();
				}
				else if(z.p.getDirection() == Direction.LEFT){
					z.g2d.setColor(Color.WHITE);
					//z.g2d.draw(z.p.getBounds());
					AffineTransform t = new AffineTransform();
					t.translate(z.p.getX()-20, z.p.getY()-80);
					t.scale(4, 4);
					z.g2d.drawImage(z.im.getPlayerPosLeft(0), t, z);
					drawNametag();
				}
			}
		}
	}
}
