package net.Archeantus.Zombinator.Audio;

import java.util.Timer;
import java.util.TimerTask;

public class Song extends Audio{
	private boolean song = false, isPlaying = false;
	private String file;
	public Song(String file){
		this.file = file;
	}
	
	public void play(final boolean loop){
		if(!isPlaying){
			isPlaying = true;
			final Timer timer = new Timer();
			final TimerTask t = new TimerTask(){
				@Override
				public void run() {
					playMusic("resources/sounds/"+file, loop);
					timer.cancel();
				}
			};timer.schedule(t, 10);
		}
		else{
			resume();
		}
	}
	
	public void pause(){
		if(song){
			song = false;
			pauseMusic();
		}
	}
	
	public void resume(){
		if(!song){
			song = true;
			resumeMusic();
		}
	}
	
	public void stop(){
		song = false;
		isPlaying = false;
		stopMusic();
	}
}
