package net.Archeantus.Zombinator.Audio;

/**
 * This class allows a quick way to utilize TinySound to play music and sound clips.
 * 
 * @author Valkryst
 * --- Last Edit 23-Sep-2013
 */
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.Archeantus.Zombinator.Zombinator;
import kuusisto.tinysound.Music;
import kuusisto.tinysound.Sound;
import kuusisto.tinysound.TinySound;

public class Audio {
   public static boolean isDirectionalSoundOn = false;
   private static HashMap<String, Sound> storedSounds = new HashMap<String, Sound>();
    private static Music music; // The music file that is currently being played.

    /**
     * Prepares TinySound for use.
     */
    public static void initialize() {
       TinySound.init();
    }
    
    /**
     * This method plays a music file.
     * @param path The path to the music file that is going to be played.
     * @param loop Whether the music file should be played on loop or not.
     */
    public static void playMusic(final String path, final boolean loop) {
      music = TinySound.loadMusic(Zombinator.class.getResource(path));
      
      if(loop) {
          music.loop();
          music.play(true);
      }
    }
    
    public static void playMusic(final String path) {
        music = TinySound.loadMusic(Zombinator.class.getResource(path));
        music.play(true);
      }

    /**
     * Set the volume of the music file that is currently being played.
     * @param volume The volume level that the currently playing music file should be played at.
     */
    public static void setVolume(final double volume) {
       music.setVolume(volume);
    }
    
    /**
     * Set the pan of the music file that is currently being played.
     * @param pan The pan that the currently playing music file should use.
     */
    public static void setPan(final double pan) {
       music.setPan(pan);
    }

    /**
     * Stops the currently playing music file.
     */
    public static void stopMusic() {
       music.stop();
    }
    
    /**
     * Pauses the currently playing music file.
     */
    public static void pauseMusic() {
       music.pause();
    }
    
    /**
     * Resumes the currently playing music file.
     */
    public static void resumeMusic() {
       music.resume();
    }
    
    /**
     * Stored the specified sound into a hashmap for use without reloading the file every use.
     * @param nameIn The name identifier of the sound to store.
     * @param soundIn The sound to store.
     */
    public static void storeSound(final String nameIn, final Sound soundIn) {
       storedSounds.put(nameIn, soundIn);
    }
    
    /**
     * This method plays a stored sound file.
     * @param nameIn The name identifier of the stored sound to play.
     * @param volumeIn The volume at which to play the stored sound.
     * @param xCoordIn the x coordinate of the object creating the
     */
    public static void playStoredSound(final String nameIn, final double volumeIn, final int xCoordIn) {
       if(isDirectionalSoundOn) {
          double pan = -1;
          
          if(xCoordIn >= 768) {
             pan = 1;
          } else if(xCoordIn >= 512) {
             pan = 0.66;
          } else if(xCoordIn >= 256) {
             pan = 0.33;
          } else {
             pan = 0;
          }
          
          storedSounds.get(nameIn).play(volumeIn, pan);
       } else {
    		  storedSounds.get(nameIn).play(volumeIn);
       }
    }
    
    /**
     * This method plays a sound file.
     * @param path The path to the music file that is going to be played.
     * @param loop Whether the music file should be played on loop or not.
     * @param loops The number of times to play the sound file.
     * @param volumeIn The volume at which to play the stored sound.
     */
    
    public static void playSound(final String path, final boolean loop, final int loops, final double volumeIn) {
      Sound sound = TinySound.loadSound(Audio.class.getResource(path));
      
      if (loop) {
          for (int i = 0; i < loops; i++) {
            sound.play();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
               Logger logger = Logger.getLogger(Audio.class.getName());
               logger.log(Level.SEVERE, e.toString(), e);
            }
          }
      } else {
          sound.play(volumeIn);
      }
    }

    /**
     * Shuts down TinySound.
     */
    public static void shutdown() {
       TinySound.shutdown();
    }
}
