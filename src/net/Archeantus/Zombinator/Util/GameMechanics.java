package net.Archeantus.Zombinator.Util;

import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Random;

import net.Archeantus.Zombinator.Zombinator;
import net.Archeantus.Zombinator.Audio.Audio;
import net.Archeantus.Zombinator.Entities.Ammo;
import net.Archeantus.Zombinator.Entities.BloodEffect;
import net.Archeantus.Zombinator.Entities.Bullet;
import net.Archeantus.Zombinator.Entities.DustEffect;
import net.Archeantus.Zombinator.Entities.GiantZombie;
import net.Archeantus.Zombinator.Entities.Item;
import net.Archeantus.Zombinator.Entities.LaserBeam;
import net.Archeantus.Zombinator.Entities.Zombie;
import net.Archeantus.Zombinator.Entities.ZombieSpawnEffect;
import net.Archeantus.Zombinator.Guns.Gun;

public class GameMechanics {
	private Zombinator z;
	
	private int stars[][] = new int[9][16];
	private int grass[] = new int[58]; 
	private boolean openplayed = false, shot = false;
	private int gun = 0, bullet = 0, delay = 4, cur = 0;
	
	public GameMechanics(Zombinator z){this.z = z;}
	
	public int[][] getStarArray(){return stars;}
	public int[] getGrassArray(){return grass;}
	
	public void generateStars(){
		Random r = new Random();
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 16; j++){
				stars[i][j] = r.nextInt(9);
			}
		}
	}
	
	public void generateGrass(){
		Random r = new Random();
		for(int i = 0; i < 58; i++){
			grass[i] = r.nextInt(4);
		}
	}
	
	public void setRegions(){
		z.leftrect = new Rectangle((-1)*z.offset, 0, ((z.WIDTH/2)-1)+z.offset, z.HEIGHT);
		
		for(Zombie zo : Zombie.zombies){
			if(zo.isAlive()){
				if(z.leftrect.contains(zo.getBounds()) && (zo.getDirection() == Direction.LEFT)){
					zo.setDirection(Direction.RIGHT);
				}
				else if(!z.leftrect.contains(zo.getBounds()) && (zo.getDirection() == Direction.RIGHT)){
					zo.setDirection(Direction.LEFT);
				}
			}
		}
		
		for(GiantZombie zo : GiantZombie.zombies){
			if(zo.isAlive()){
				if(z.leftrect.contains(zo.getBounds()) && (zo.getDirection() == Direction.LEFT)){
					zo.setDirection(Direction.RIGHT);
				}
				else if(!z.leftrect.contains(zo.getBounds()) && (zo.getDirection() == Direction.RIGHT)){
					zo.setDirection(Direction.LEFT);
				}
			}
		}
	}
	
	public void initNextWave(){
		z.waveNum++;
		z.colvar = 255;
		z.wavetext = true;
		spawnZombies();
		z.zombiesToSpawn += 3;
	}
	
	public void spawnZombies(){
		for(int i = 0; i < z.zombiesToSpawn; i++){
			Audio.playStoredSound("rising", 0.5, 0);
			Random r = new Random();
			int x = r.nextInt(5300)+100;
			Point p = new Point(x-z.offset, 600);
			if(z.leftrect.contains(p)){
				new ZombieSpawnEffect(x, 600, 2);
			}
			else{
				new ZombieSpawnEffect(x, 600, 1);
			}
		}
		new GiantZombie(1800, 600, Direction.RIGHT);
	}

	public void checkCollisions(){
		for(Bullet b : Bullet.bullets){
			if(b.EntityIsAlive()){
				for(Zombie zo : Zombie.zombies){
					if(zo.isAlive()){
						if(zo.getBounds().contains(b.getBounds())){
							zo.harm(b.getDamage());
							b.setAlive(false);
							new BloodEffect(zo.getX(), b.getY());
							if(zo.getDirection() == Direction.LEFT){
								zo.setX(zo.getX()+10);
								if(zo.getHealth() <= 0){
									new DustEffect(zo.getX(), b.getY()-20);
									zo.setDead();
									z.score += 100;
									spawnAmmo(zo.getX());
									spawnCoins(zo.getX());
									spawnOrbs(zo.getX());
								}
							}
							else if(zo.getDirection() == Direction.RIGHT){
								zo.setX(zo.getX()-10);
								if(zo.getHealth() <= 0){
									new DustEffect(zo.getX(), b.getY()-20);
									zo.setDead();
									z.score += 100;
									spawnAmmo(zo.getX());
									spawnCoins(zo.getX());
									spawnOrbs(zo.getX());
								}
							}
						}
					}
				}
				
				for(GiantZombie zo : GiantZombie.zombies){
					if(zo.isAlive()){
						if(zo.getBounds().contains(b.getBounds())){
							zo.harm(b.getDamage());
							b.setAlive(false);
							new BloodEffect(zo.getX(), b.getY());
							if(zo.getDirection() == Direction.LEFT){
								zo.setX(zo.getX()+10);
								if(zo.getHealth() <= 0){
									new DustEffect(zo.getX(), b.getY()-40);
									zo.setDead();
									z.score += 250;
									spawnAmmo(zo.getX());
									spawnCoins(zo.getX());
									spawnOrbs(zo.getX());
								}
							}
							else if(zo.getDirection() == Direction.RIGHT){
								zo.setX(zo.getX()-10);
								if(zo.getHealth() <= 0){
									new DustEffect(zo.getX(), b.getY()-40);
									zo.setDead();
									z.score += 250;
									spawnAmmo(zo.getX());
									spawnCoins(zo.getX());
									spawnOrbs(zo.getX());
								}
							}
						}
					}
				}
			}
		}
		
		for(LaserBeam b : LaserBeam.bullets){
			if(b.EntityIsAlive()){
				for(Zombie zo : Zombie.zombies){
					if(zo.isAlive()){
						if(zo.getBounds().contains(b.getBounds())){
							zo.harm(b.getDamage());
							b.setAlive(false);
							new BloodEffect(zo.getX(), b.getY());
							if(zo.getDirection() == Direction.LEFT){
								zo.setX(zo.getX()+10);
								if(zo.getHealth() <= 0){
									new DustEffect(zo.getX(), b.getY()-20);
									zo.setDead();
									z.score += 100;
									spawnAmmo(zo.getX());
									spawnCoins(zo.getX());
									spawnOrbs(zo.getX());
								}
							}
							else if(zo.getDirection() == Direction.RIGHT){
								zo.setX(zo.getX()-10);
								if(zo.getHealth() <= 0){
									new DustEffect(zo.getX(), b.getY()-20);
									zo.setDead();
									z.score += 100;
									spawnAmmo(zo.getX());
									spawnCoins(zo.getX());
									spawnOrbs(zo.getX());
								}
							}
						}
					}
				}
				
				for(GiantZombie zo : GiantZombie.zombies){
					if(zo.isAlive()){
						if(zo.getBounds().contains(b.getBounds())){
							zo.harm(b.getDamage());
							b.setAlive(false);
							new BloodEffect(zo.getX(), b.getY());
							if(zo.getDirection() == Direction.LEFT){
								zo.setX(zo.getX()+10);
								if(zo.getHealth() <= 0){
									new DustEffect(zo.getX(), b.getY()-40);
									zo.setDead();
									z.score += 250;
									spawnAmmo(zo.getX());
									spawnCoins(zo.getX());
									spawnOrbs(zo.getX());
								}
							}
							else if(zo.getDirection() == Direction.RIGHT){
								zo.setX(zo.getX()-10);
								if(zo.getHealth() <= 0){
									new DustEffect(zo.getX(), b.getY()-20);
									zo.setDead();
									z.score += 250;
									spawnAmmo(zo.getX());
									spawnCoins(zo.getX());
									spawnOrbs(zo.getX());
								}
							}
						}
					}
				}
			}
		}
		
		for(Zombie z1 : Zombie.zombies){
			if(z1.isAlive()){
				if(z1.getBounds().intersects(z.p.getBounds())){
					if(!z.p.isInjured() && z.p.isAlive()){
						z.p.harm(1);
						z.p.setInjured(true);
						new BloodEffect((z.WIDTH/2)+z.offset, z.p.getY()-67);
					}
					z1.setMoving(false);
					z1.setBox(z1.getX()-z.offset);
				}
				else{
					if(!z1.isMoving()){
						z1.setMoving(true);
					}
				}
			}
		}
		
		for(GiantZombie z1 : GiantZombie.zombies){
			if(z1.isAlive()){
				if(z1.getBounds().intersects(z.p.getBounds())){
					if(!z.p.isInjured() && z.p.isAlive()){
						z.p.harm(2);
						z.p.setInjured(true);
						new BloodEffect((z.WIDTH/2)+z.offset, z.p.getY()-67);
					}
					z1.setMoving(false);
					z1.setBox(z1.getX()-z.offset);
				}
				else{
					if(!z1.isMoving()){
						z1.setMoving(true);
					}
				}
			}
		}
		
		for(Ammo a : Ammo.ammoEntities){
			if(a.EntityIsAlive()){
				if(a.getBounds(z.offset).intersects(z.p.getBounds())){
					a.setAlive(false);
					setRounds(z.p.getCurrent(), getRounds(z.p.getCurrent())+a.getAmount());
					Audio.playStoredSound("pickup", 0.1, 0);
				}
			}
		}
		
		for(Item i : Item.itemEntities){
			if(i.EntityIsAlive()){
				if(i.getBounds(z.offset).intersects(z.p.getBounds())){
					i.setAlive(false);
					Audio.playStoredSound("pickup", 0.1, 0);
					if(i.getType() == 1){
						z.coins += i.getAmount();
						z.score += 15;
					}
					else if(i.getType() == 2){
						z.exp += i.getAmount();
						z.score += 15;
					}
				}
			}
		}
		
		if(z.c.isOpen() && !z.c.getBounds(z.offset).intersects(z.p.getBounds())){
			z.c.toggle();
		}
	}
	
	public void pushBack(int dir){
		Rectangle abounds = new Rectangle(z.offset*(-1), 0, z.mapSize, z.HEIGHT);
		Rectangle pbounds = z.p.getBounds();
		int amount = 0;
		while(!(abounds.contains(pbounds))){
			if(dir == Direction.LEFT){
				amount += 5;
				pbounds.setBounds((int)pbounds.getX()+amount, (int)pbounds.getY(), (int)pbounds.getWidth(), (int)pbounds.getHeight());
				if(abounds.contains(pbounds)){
					z.offset += amount;
					//Logger.log("You have reached the border of this map.");
					break;
				}
			}
			else if(dir == Direction.RIGHT){
				amount += 5;
				pbounds.setBounds((int)pbounds.getX()-amount, (int)pbounds.getY(), (int)pbounds.getWidth(), (int)pbounds.getHeight());
				if(abounds.contains(pbounds)){
					z.offset -= amount;
					//Logger.log("You have reached the border of this map.");
					break;
				}
			}
		}
	}
	
	public void updateBullet(){
		if(shot && !(bullet <= 0)){
			cur++;
			if(cur >= delay){
				cur = 0;
				if(getBullets(gun) >= 1){
					bullet--;
					if(z.p.getDirection() == Direction.LEFT){
						if(bullet <= 0){shot = false;}
						new Bullet(z.p.getX()-50, z.p.getY()-47, getDamage(z.p.getCurrent()), z.p.getDirection());
						subBullet(z.p.getCurrent());
						Audio.playStoredSound("fire", 1.0, 0);
						if(checkPos(getKnockback(z.p.getCurrent()))){
							z.offset += getKnockback(z.p.getCurrent());
						}
					}
					else if(z.p.getDirection() == Direction.RIGHT){
						if(bullet <= 0){shot = false;}
						new Bullet(z.p.getX()+30, z.p.getY()-42, getDamage(z.p.getCurrent()), z.p.getDirection());
						subBullet(z.p.getCurrent());
						Audio.playStoredSound("fire", 1.0, 0);
						if(checkPos(getKnockback(z.p.getCurrent()))){
							z.offset -= getKnockback(z.p.getCurrent());
						}
					}
				}
				else{
					bullet = 0;
					shot = false;
				}
			}
		}
	}
	
	public void Shoot(){
		if(!(getBullets(z.p.getCurrent()) <= 0)){
			if(!z.p.isReloading() &&  !z.reload){
				if(z.p.getCurrent() != 11){
					if(z.p.getDirection() == Direction.LEFT){
						if(!shot){
							if(getFirerate(z.p.getCurrent()) > 1){
								gun = z.p.getCurrent();
								bullet = getFirerate(gun)-1;
								shot = true;
							}
							new Bullet(z.p.getX()-50, z.p.getY()-47, getDamage(z.p.getCurrent()), z.p.getDirection());
							subBullet(z.p.getCurrent());
							Audio.playStoredSound("fire", 1.0, 0);
							if(checkPos(getKnockback(z.p.getCurrent()))){
								z.offset += getKnockback(z.p.getCurrent());
							}
						}
					}
					else if(z.p.getDirection() == Direction.RIGHT){
						if(!shot){
							if(getFirerate(z.p.getCurrent()) > 1){
								gun = z.p.getCurrent();
								bullet = getFirerate(gun)-1;
								shot = true;
							}
							new Bullet(z.p.getX()+30, z.p.getY()-42, getDamage(z.p.getCurrent()), z.p.getDirection());
							subBullet(z.p.getCurrent());
							Audio.playStoredSound("fire", 1.0, 0);
							if(checkPos(getKnockback(z.p.getCurrent()))){
								z.offset -= getKnockback(z.p.getCurrent());
							}
						}
					}
				}
				else if(z.p.getCurrent() == 11){
					if(z.p.getDirection() == Direction.LEFT){
						if(!shot){
							if(getFirerate(z.p.getCurrent()) > 1){
								gun = z.p.getCurrent();
								bullet = getFirerate(gun)-1;
								shot = true;
							}
							new LaserBeam(z.p.getX()-50, z.p.getY()-52, getDamage(z.p.getCurrent()), z.p.getDirection());
							subBullet(z.p.getCurrent());
							Audio.playStoredSound("laser", 1.0, 0);
							if(checkPos(getKnockback(z.p.getCurrent()))){
								z.offset += getKnockback(z.p.getCurrent());
							}
						}
					}
					else if(z.p.getDirection() == Direction.RIGHT){
						if(!shot){
							if(getFirerate(z.p.getCurrent()) > 1){
								gun = z.p.getCurrent();
								bullet = getFirerate(gun)-1;
								shot = true;
							}
							new LaserBeam(z.p.getX()+30, z.p.getY()-38, getDamage(z.p.getCurrent()), z.p.getDirection());
							subBullet(z.p.getCurrent());
							Audio.playStoredSound("laser", 1.0, 0);
							if(checkPos(getKnockback(z.p.getCurrent()))){
								z.offset -= getKnockback(z.p.getCurrent());
							}
						}
					}
				}
			}
		}
	}

	public void spawnAmmo(int x){
		Random r = new Random();
		int i  = r.nextInt(4)+1;
		if(i == 1){
			new Ammo(x, 570, r.nextInt(5)+1);
		}
	}
	
	public void spawnCoins(int x){
		Random r = new Random();
		new Item(x+10, 570, r.nextInt(10)+1, 1, false);
	}
	
	public void spawnOrbs(int x){
		Random r = new Random();
		new Item(x-10, 570, r.nextInt(25)+1, 2, false);
	}
	
	public void checkPosition(){
		Rectangle abounds = new Rectangle(z.offset*(-1), 0, z.mapSize, z.HEIGHT);
		if(!abounds.contains(z.p.getBounds())){
			z.g.pushBack(z.p.getDirection());
		}
	}
	
	public boolean checkPos(int k){
		Rectangle abounds = new Rectangle(z.offset*(-1), 0, z.mapSize, z.HEIGHT);
		Rectangle pbounds = z.p.getBounds();
		if(z.p.getDirection() == Direction.LEFT){
			pbounds.setBounds((int)pbounds.getX()+k, (int)pbounds.getY(), (int)pbounds.getWidth(), (int)pbounds.getHeight());
			if(abounds.contains(pbounds)){
				return true;
			}
			else{
				return false;
			}
		}
		else if(z.p.getDirection() == Direction.RIGHT){
			pbounds.setBounds((int)pbounds.getX()-k, (int)pbounds.getY(), (int)pbounds.getWidth(), (int)pbounds.getHeight());
			if(abounds.contains(pbounds)){
				return true;
			}
			else{
				return false;
			}
		}
		return false;
	}
	
	public void movePlayer(){
		Rectangle abounds = new Rectangle(z.offset*(-1), 0, z.mapSize, z.HEIGHT);
		if(z.p.isMoving()){
			if(abounds.contains(z.p.getBounds())){
				if(z.p.getDirection() == Direction.RIGHT){
					z.offset += z.p.getSpeed();
				}
				else if(z.p.getDirection() == Direction.LEFT){
					z.offset -= z.p.getSpeed();
				}
			}
		}
	}
	
	public void playerJump(){
		if(z.p.isJumping()){
			z.p.jumpPhase();
		}
	}
	
	public void Switch(){
		if(!(z.p.getCurrent() == z.p.getPrimary()) && !z.reload && !shot){
			z.p.setCurrent(z.p.getPrimary());
		}
		else if(!(z.p.getCurrent() == z.p.getSecondary()) && !z.reload && !shot){
			z.p.setCurrent(z.p.getSecondary());
		}
	}
	
	public void reloadIfNeeded(){
		if(z.reload){
			z.currRel++;
			if(z.currRel >= getReloadTime(z.p.getCurrent())){
				z.currRel = 0;
				z.reload = false;
				Audio.playStoredSound("reload2", 1.0, 0);
				openplayed = false;
			}
		}
		
		if(z.p.isReloading()){
			if(!(getBullets(z.p.getCurrent()) >= getClipSize(z.p.getCurrent()))){
				if(!(getRounds(z.p.getCurrent()) <= 0)){
					for(Gun g : Gun.guns){
						if(g.getGunId() == z.p.getCurrent()){
							g.setBullets(g.getBullets()+1);
							g.setRounds(g.getRounds()-1);
							if(!openplayed){openplayed = true;Audio.playStoredSound("reload1", 1.0, 0);}
						}
					}
				}
				else{
					z.p.setReloading(false);
				}
			}
			else{
				z.p.setReloading(false);
			}
		}
	}
	
	public void checkZombies(){
		int j = 0;
		for(Zombie zo : Zombie.zombies){
			if(zo.isAlive()){
				j++;
			}
		}
		
		for(ZombieSpawnEffect zi : ZombieSpawnEffect.getSpawning){
			if(zi.EntityIsAlive()){
				j++;
			}
		}
		
		for(GiantZombie zg : GiantZombie.zombies){
			if(zg.isAlive()){
				j++;
			}
		}
		
		if(j == 0){
			if(!z.clock.wavedone){z.clock.wavedone = true;Audio.playStoredSound("wave_done", 0.5, 0);}
		}
	}
	
	public void generateNoises(){
		int j = 0;
		for(Zombie z : Zombie.zombies){
			if(z.isAlive()){
				j++;
			}
		}
		
		if(j > 0){
			Random r = new Random();
			int er = r.nextInt(100);
			switch(er){
				case 17:
					Audio.playStoredSound("moan", 0.1, 0);
					break;
			}
		}
	}
	
	public Gun getGun(int i){
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				return g;
			}
			else{
				return null;
			}
		}
		return null;
	}
	
	public void subBullet(int i){
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				g.setBullets(g.getBullets()-1);
				if(g.getBullets() <= 0){
					z.p.setReloading(true);
					z.reload = true;
				}
			}
		}
	}
	
	public void randomizedGun(){
		Random r = new Random();
		int i = r.nextInt(Gun.guns.size());
		int ro = r.nextInt(65)+1;
		
		if(!(i == z.p.getPrimary()) && !(i == z.p.getSecondary())){
			if(z.p.getCurrent() == z.p.getPrimary()){
				z.p.setCurrent(i);
				z.p.setPrimary(i);
				setBullets(i, getClipSize(i));
				setRounds(i, ro);
			}
			else if(z.p.getCurrent() == z.p.getSecondary()){
				z.p.setCurrent(i);
				z.p.setSecondary(i);
				setBullets(i, getClipSize(i));
				setRounds(i, ro);
			}
		}
		else{
			randomizedGun();
		}
	}
	
	public int getStringWidth(String s){
		FontMetrics fm = z.g2d.getFontMetrics();
		return fm.stringWidth(s);
	}
	
	public int getDamage(int i){
		int e = 0;
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				e = g.getDamage();
			}
		}
		return e;
	}
	
	public int getKnockback(int i){
		int e = 0;
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				e = g.getKnockback();
			}
		}
		return e;
	}
	
	public int getClipSize(int i){
		int e = 0;
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				e = g.getClipSize();
			}
		}
		return e;
	}
	
	public int getReloadTime(int i){
		int e = 0;
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				e = g.getReloadTime();
			}
		}
		return e;
	}
	
	public int getRounds(int i){
		int e = 0;
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				e = g.getRounds();
			}
		}
		return e;
	}
	
	public void setRounds(int gunId, int i){
		for(Gun g : Gun.guns){
			if(g.getGunId() == gunId){
				g.setRounds(i);
			}
		}
	}
	
	public int getBullets(int i){
		int e = 0;
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				e = g.getBullets();
			}
		}
		return e;
	}
	
	public int setBullets(int gunId, int i){
		for(Gun g : Gun.guns){
			if(g.getGunId() == gunId){
				g.setBullets(i);
			}
		}
		return i;
	}
	
	public int getFirerate(int i){
		int e = 0;
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				e = g.getFireRate();
			}
		}
		return e;
	}

	public String getName(int i){
		String n = null;
		for(Gun g : Gun.guns){
			if(g.getGunId() == i){
				n = g.getName();
			}
		}
		return n;
	}
}
