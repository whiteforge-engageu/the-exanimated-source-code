package net.Archeantus.Zombinator.Util;

import java.util.Timer;
import java.util.TimerTask;

import net.Archeantus.Zombinator.Zombinator;

public class Clock {
	
	private Zombinator z;
	private Timer timer = new Timer();
	private int seconds = 0, timeleft = 20;
	public boolean wavedone = false;
	
	public Clock(Zombinator z){
		this.z = z;
	}
	
	public void startTimer(){
		timer.schedule(new TimerTask(){

			@Override
			public void run() {
				addSec();
				countDown();
			}
			
		}, 0, 1000);
	}
	
	public void addSec(){
		seconds++;
	}
	
	public void countDown(){
		if(wavedone){
			if(timeleft > 0){
				timeleft--;
			}
			else{
				wavedone = false;
				timeleft = 20;
				z.g.initNextWave();
			}
		}
	}
	
	public String getTimeLeft(){
		StringBuilder s = new StringBuilder();
		if(timeleft < 10){
			s.append("0").append(timeleft);
			return s.toString();
		}
		else{
			s.append(timeleft);
			return s.toString();
		}
	}
	
	public String getClockSeconds(){
		StringBuilder s = new StringBuilder();
		if(seconds%60 < 10){
			s.append("0").append(seconds%60);
			return s.toString();
		}
		else{
			s.append(seconds%60);
			return s.toString();
		}
	}
	
	public String getClockMinutes(){
		StringBuilder s = new StringBuilder();
		if(seconds/60 < 10){
			s.append("0").append(seconds/60);
			return s.toString();
		}
		else{
			s.append(seconds/60);
			return s.toString();
		}
	}
}
