package net.Archeantus.Zombinator.Util;

public class Direction {
	private boolean left, right;
	public static int LEFT = 1, RIGHT = 2;
	
	public int getDirection(){if(left){return LEFT;}else if(right){return RIGHT;}else{return RIGHT;}}
	
	public void setDirection(int i){
		if(i == LEFT){
			left = true;
			right = false;
		}
		else if(i == RIGHT){
			left = false;
			right = true;
		}
	}
}
