package net.Archeantus.Zombinator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import kuusisto.tinysound.TinySound;
import net.Archeantus.Bayonet.BayonetServerFramework;
import net.Archeantus.Zombinator.Audio.Audio;
import net.Archeantus.Zombinator.Audio.Song;
import net.Archeantus.Zombinator.Entities.Chest;
import net.Archeantus.Zombinator.Entities.Player;
import net.Archeantus.Zombinator.Entities.Structure;
import net.Archeantus.Zombinator.Guns.Gun;
import net.Archeantus.Zombinator.Util.Clock;
import net.Archeantus.Zombinator.Util.GameMechanics;
import net.Archeantus.Zombinator.input.KeyboardInput;
import net.Archeantus.Zombinator.input.MouseInput;

public class Zombinator extends JFrame implements Runnable{
	
	private static final long serialVersionUID = -8206077393921420720L;
	public int x = 0, y = 0;
	public int WIDTH = 1280, HEIGHT = 720, currRel = 0, offset = 0, score = 0, waveNum = 0, colvar = 255, zombiesToSpawn = 3, timeNextShow  = 100, mapSize = 6144, titleCount = 0, coins = 0, exp = 0, price = 85, primd = 0, sec = 7;
	public static int count = 0, rows = 0, tcount = 0, trows = 0, gunSel = -1;
	public final int fpsMax = 30, skipFrames = 1000/fpsMax;
	public int frameCount = 0, frameRate = 0;
	public long startTime;
	public boolean reload = false, ingame = false, Primary = true, select = true, menu = true, title = true, spawned = false, ambiancestart = false, clockstarted = false, wavetext = false, eclipse = false;
	public String version = "Alpha 1.1.0", os = "";
	public static String player = "Player";
	public Rectangle screen = new Rectangle(-50, 0, WIDTH+100, HEIGHT);
	public boolean nheader = true;
	
	Font f;
	Thread game;
	public Graphics2D g2d;
	BufferedImage buffer;
	KeyboardInput key;
	MouseInput mouse;
	public Clock clock;
	BayonetServerFramework b;
	public Chest c;
	public ImageWarehouse im;
	public Rectangle leftrect;
	public GameMechanics g;
	public Whiteboard w;
	public Player p;
	public Song walking, ambiance;
	
	public static void main(String[] args){new Zombinator(); if(args.length > 0){player = args[0];}}
	
	public Zombinator(){
		super("The Exanimated");
		this.setSize(WIDTH, HEIGHT);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		buffer = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g2d = buffer.createGraphics();
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (dim.width-WIDTH)/2;
		int y = (dim.height-HEIGHT)/2;
		
		this.setLocation(x, y);
		
		key = new KeyboardInput(this);
		mouse = new MouseInput(this);
		im = new ImageWarehouse(this);
		w = new Whiteboard(this);
		g = new GameMechanics(this);
		c = new Chest(1400, 0);
		b = new BayonetServerFramework();
		clock = new Clock(this);
		
		BufferedImage icon = null;
		try {icon = ImageIO.read(this.getClass().getResource("resources/images/icon.png"));} catch (IOException e1) {e1.printStackTrace();}
		
		ImageIcon img = new ImageIcon(icon);
		
		this.setIconImage(img.getImage());
		
		Audio.initialize();
		Audio.storeSound("reload1", TinySound.loadSound(this.getClass().getResource("resources/sounds/lockload.wav")));
		Audio.storeSound("reload2", TinySound.loadSound(this.getClass().getResource("resources/sounds/reload.wav")));
		Audio.storeSound("fire", TinySound.loadSound(this.getClass().getResource("resources/sounds/shoot.wav")));
		Audio.storeSound("jump", TinySound.loadSound(this.getClass().getResource("resources/sounds/Jump.wav")));
		Audio.storeSound("moan", TinySound.loadSound(this.getClass().getResource("resources/sounds/moan.wav")));
		Audio.storeSound("poltergeist", TinySound.loadSound(this.getClass().getResource("resources/sounds/poltergeist.wav")));
		Audio.storeSound("rising", TinySound.loadSound(this.getClass().getResource("resources/sounds/rising.wav")));
		Audio.storeSound("pickup", TinySound.loadSound(this.getClass().getResource("resources/sounds/pickup.wav")));
		Audio.storeSound("chest_open", TinySound.loadSound(this.getClass().getResource("resources/sounds/chest_open.wav")));
		Audio.storeSound("chest_close", TinySound.loadSound(this.getClass().getResource("resources/sounds/chest_close.wav")));
		Audio.storeSound("random", TinySound.loadSound(this.getClass().getResource("resources/sounds/random.wav")));
		Audio.storeSound("laser", TinySound.loadSound(this.getClass().getResource("resources/sounds/laser_shoot.wav")));
		Audio.storeSound("wave_done", TinySound.loadSound(this.getClass().getResource("resources/sounds/wave_done.wav")));
		Audio.storeSound("blip", TinySound.loadSound(this.getClass().getResource("resources/sounds/Blip.wav")));
		Audio.storeSound("blip2", TinySound.loadSound(this.getClass().getResource("resources/sounds/Blip2.wav")));
		walking = new Song("walking.wav");
		ambiance = new Song("ambiance.wav");
		
		this.loadFont();
		g.generateStars();
		g.generateGrass();
		
		this.addKeyListener(key);
		this.addMouseListener(mouse);
		this.addMouseMotionListener(mouse);
		
		this.loadGuns();
		
		new Structure(1000, 0, 311, 260, 0);
		new Structure(1800, 0, 464, 193, 1);
		new Structure(-100, 40, 113, 80, 2);
		new Structure(6120, 40, 113, 80, 2);
		
		game = new Thread(this);
		game.start();
		
		startTime = System.currentTimeMillis();
		os = System.getProperty("os.name");
		if(os.equalsIgnoreCase("Linux") || os.toLowerCase().contains("mac"))nheader = false;
		System.out.println("Java version: "+System.getProperty("java.version"));
		System.out.println("Operating System: "+os);
		System.out.println("OS Version: "+System.getProperty("os.version"));
		System.out.println("Game Version: "+version);
	}
	
	@Override
	public void run() {
		long tickCount = System.currentTimeMillis();
		long sleep_time = 0;
		Thread t = Thread.currentThread();
		while(t == game){
			repaint();
			tickCount += skipFrames;
			sleep_time = tickCount - System.currentTimeMillis();
			if(sleep_time >= 0){
				try {Thread.sleep(sleep_time);} catch (InterruptedException e) {e.printStackTrace();}
			}
		}	
	}
	
	public void paint(Graphics gr){
		gr.drawImage(buffer, 0, 0, this);
		
		frameCount++;
		if(System.currentTimeMillis() > startTime + 1000){
			startTime = System.currentTimeMillis();
			frameRate = frameCount;
			frameCount = 0;
		}
		
		if(g2d != null && w != null){
			g2d.setColor(Color.CYAN);
			g2d.fillRect(0, 0, WIDTH, HEIGHT);
			
			if(!title){
				if(!menu){
					if(!select){
						if(ingame){
							if(!ambiancestart){ambiancestart = true;ambiance.play(true);}
							if(!clockstarted){clock.startTimer();clockstarted = true;}
							g.setRegions();
							
							w.drawStars();
							
							w.drawMoon();
							
							w.drawGrass();
							
							g.movePlayer();
							
							g.playerJump();
							
							w.drawStructures();
							
							w.drawChest();
							
							w.drawBullets();
							
							w.renderGun();
							
							w.drawPlayer();
							
							w.drawZombies();
							
							w.drawEntities();
							
							w.drawBlood();
							
							w.drawDust();
							
							w.drawHealth();
							
							w.drawDpad();
							
							w.drawGun();
							
							w.drawFPS();
							
							w.drawScore();
							
							w.drawTime();
							
							w.drawWaveText();
							
							//w.drawMessages();
							
							g.reloadIfNeeded();
							
							g.updateBullet();
							
							g.checkCollisions();
							
							p.regenHealth();
							
							p.checkInjured();
							
							g.checkPosition();
							
							g.generateNoises();
							
							if(!spawned){g.initNextWave();spawned = true;}
							
							g.checkZombies();
							
							if(p.getHealth() <= 0){p.setAlive(false);}
							
							if(!p.isAlive()){w.drawDeathScreen();}
						}
					}
					else{
						g2d.setColor(Color.BLACK);
						g2d.fillRect(0, 0, WIDTH, HEIGHT);
						w.drawMenu();
					}
				}
				else{
					g2d.setColor(Color.BLACK);
					g2d.fillRect(0, 0, WIDTH, HEIGHT);
					
					w.drawBackground();
					
					g2d.drawImage(im.getLogo(), (WIDTH-im.getLogo().getWidth())/2, 150, this);
					g2d.setColor(Color.WHITE);
					g2d.setFont(g2d.getFont().deriveFont(14F));g2d.drawString("Copyright 2015. Archeantus.net", 20, 710);
					g2d.setFont(g2d.getFont().deriveFont(14F));g2d.drawString(version, (WIDTH-g.getStringWidth(version))-20, 710);
					w.drawMenuButtons();
				}
			}
			else{
				if(titleCount > 500){
					title = false;
				}
				else{
					g2d.setColor(Color.WHITE);
					g2d.fillRect(0, 0, WIDTH, HEIGHT);
					g2d.drawImage(im.getTitle(), (WIDTH-256)/2, (HEIGHT-256)/2, this);
					titleCount++;
				}
			}
		}
	}
	
	private void loadGuns(){
		//new Gun(int rounds, int clipSize, int dam, int fireRate, int knockback, int relTime, int id, String name, boolean Secondary){
		new Gun(32, 16, 3, 4, 10, 100, 0, "AK12", false);
		new Gun(48, 12, 3, 1, 10, 100, 1, "Battle Rifle", false);
		new Gun(32, 16, 3, 4, 10, 100, 2, "SCAR L", false);
		new Gun(38, 6, 3, 1, 10, 100, 3, "Barret .50 Cal", false);
		new Gun(58, 2, 5, 1, 25, 100, 4, "Trench Rifle", false);
		new Gun(58, 4, 5, 2, 25, 100, 5, "Spas12", false);
		new Gun(32, 8, 3, 4, 10, 100, 6, "MSR", false);
		new Gun(24, 6, 2, 1, 10, 50, 7, "9mm", true);
		new Gun(28, 10, 4, 1, 10, 45, 8, "Glock 18C", true);
		new Gun(18, 6, 3, 1, 15, 85, 9, "Magnum", true);
		new Gun(20, 10, 6, 1, 15, 50, 10, "Python", true);
		new Gun(10, 2, 10, 1, 10, 50, 11, "Laser Blaster", false);
		new Gun(48, 30, 2, 4, 10, 70, 12, "MP5", false);
		new Gun(36, 9, 2, 3, 10, 60, 13, "Uzi (MP-2)", true);
		new Gun(48, 16, 4, 1, 15, 85, 14, "Remington 870", false);
		new Gun(60, 24, 3, 4, 10, 40, 15, "AK-47", false);
		new Gun(60, 60, 4, 6, 20, 180, 16, "M60 Machine Gun", false);
		new Gun(40, 10, 4, 1, 20, 60, 17, "Desert Eagle", true);
	}
	
	public void loadFont(){
		InputStream is = this.getClass().getResourceAsStream("resources/font.ttf");
		try {f = Font.createFont(Font.TRUETYPE_FONT, is);} catch (FontFormatException e1) {e1.printStackTrace();} catch (IOException e1) {e1.printStackTrace();}
		try {is.close();} catch (IOException e) {e.printStackTrace();}
		g2d.setFont(f.deriveFont(14F));
	}
}
