package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;
import java.util.ArrayList;

public class Item {
	public static ArrayList<Item> itemEntities = new ArrayList<Item>();
	
	private int x, y, amount, type, frame = 0, maxframe = 3, frameCount = 0, frameDelay = 3;
	private boolean isAlive, isStationary;
	
	public int getX(){return this.x;}
	public int getY(){return this.y;}
	public int getAmount(){return this.amount;}
	public int getType(){return this.type;}
	public int getFrame(){return this.frame;}
	public Rectangle getBounds(int offset){Rectangle r = new Rectangle(getX()-offset, getY()-32, 16, 16);return r;}
	public boolean EntityIsAlive(){return this.isAlive;}
	public boolean isStationary(){return this.isStationary;}
	
	public void setX(int i){this.x = i;}
	public void setY(int i){this.y = i;}
	public void setAmount(int i){this.amount = i;}
	public void setAlive(boolean i){this.isAlive = i;}
	public void updateEntity(){if((getY() < 612) && !isStationary()){setY(getY()+10);} frameCount++; if(frameCount > frameDelay){frameCount = 0;frame++; if(frame > maxframe){frame = 0;}}}
	
	public Item(int x, int y, int a, int t, boolean is){
		setX(x);
		setY(y);
		setAmount(a);
		this.type = t;
		this.isStationary = is;
		setAlive(true);
		itemEntities.add(this);
	}
}
