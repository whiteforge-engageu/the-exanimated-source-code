package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;
import java.util.ArrayList;

public class Ammo {
	public static ArrayList<Ammo> ammoEntities = new ArrayList<Ammo>();
	
	private int x, y, amount;
	private boolean isAlive;
	
	public int getX(){return this.x;}
	public int getY(){return this.y;}
	public int getAmount(){return this.amount;}
	public Rectangle getBounds(int offset){Rectangle r = new Rectangle(getX()-offset, getY()-32, 32, 32);return r;}
	public boolean EntityIsAlive(){return isAlive;}
	
	public void setX(int i){this.x = i;}
	public void setY(int i){this.y = i;}
	public void setAmount(int i){this.amount = i;}
	public void setAlive(boolean i){this.isAlive = i;}
	public void updateEntity(){if((getY() < 600)){setY(getY()+10);}}
	
	public Ammo(int x, int y, int a){
		setX(x);
		setY(y);
		setAmount(a);
		setAlive(true);
		ammoEntities.add(this);
	}
}
