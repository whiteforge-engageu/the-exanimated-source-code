package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;

import net.Archeantus.Zombinator.Audio.Audio;
import net.Archeantus.Zombinator.Util.Direction;

public class Player extends BasicEntity{
	
	private int frCount = 0, frDelay = 2, frame = 0, frames = 3, jcurr = 0, gunId1, gunId2, currGun, injurCount = 0, injurDel = 50, regenCount = 0, regenDel = 500, flashCount = 0, flashDel = 5, jumpHeight = 100;
	private Rectangle bounds;
	private Direction dir = new Direction();
	private boolean isMoving, isJumping, isReloading, max = false, injured = false;
	
	public Rectangle getBounds(){bounds = new Rectangle(getX()-20, getY()-80, 40, 80);return bounds;}
	public int getDirection(){return dir.getDirection();}
	public int getFrame(){return frame;}
	public int getPrimary(){return gunId1;}
	public int getSecondary(){return gunId2;}
	public int getCurrent(){return currGun;}
	public boolean isMoving(){return isMoving;}
	public boolean isJumping(){return isJumping;}
	public boolean isReloading(){return isReloading;}
	public boolean isInjured(){return injured;}
	
	public void setPrimary(int i){this.gunId1 = i;}
	public void setSecondary(int i){this.gunId2 = i;}
	public void setCurrent(int i){this.currGun = i;}
	public void setDirection(int i){dir.setDirection(i);}
	public void setMoving(boolean s){this.isMoving = s;}
	public void setJumping(boolean s){this.isJumping = s;}
	public void setJumpingHeight(int j){this.jumpHeight = j;}
	public void setReloading(boolean s){this.isReloading = s;}
	public void setInjured(boolean s){this.injured = s;}
	public void checkInjured(){if(isInjured()){injurCount++;if(injurCount > injurDel){injurCount = 0;setInjured(false);}} checkFlash();}
	public void checkFlash(){if(flashed()){flashCount++;if(flashCount > flashDel){flashCount = 0;flashed = false;}}}
	public void updateFrame(){frCount++;if(frCount > frDelay){frCount = 0;frame++;if(frame > frames){frame = 0;}}}
	public void regenHealth(){if(isAlive()){if(!isInjured() && !(getHealth() >= getMaxHealth())){regenCount++;if(regenCount > regenDel){regenCount = 0;setHealth(getHealth()+1);checkFlash();}}}}
	public void moveRight(){setMoving(true);setDirection(Direction.RIGHT);}
	public void moveLeft(){setMoving(true);setDirection(Direction.LEFT);}
	public void stopMoving(){setMoving(false);}
	public void Jump(){if(!isJumping()){setJumping(true);jcurr += 20;setY(getY()-20);Audio.playStoredSound("jump", 0.5, 0);}}
	public void jumpPhase(){if(max){if(isJumping()){setY(getY()+20);jcurr -= 20;if(getY() >= 600){setJumping(false);max = false;jcurr = 0;}}}else{if(!(jcurr >= jumpHeight)){jcurr += 20;setY(getY()-20);}else{max = true;}}}
	
	public Player(int x, int y, int d, int prim, int sec){
		setX(x);
		setY(y);
		setDirection(d);
		setAlive(true);
		setSpeed(10);
		setMaxHealth(10);
		setHealth(10);
		setJumpingHeight(120);
		setMoving(false);
		setJumping(false);
		setReloading(false);
		setPrimary(prim);
		setSecondary(sec);
		setCurrent(prim);
	}
	
}
