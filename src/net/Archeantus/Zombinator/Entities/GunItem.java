package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;
import java.util.ArrayList;

import net.Archeantus.Zombinator.Zombinator;
import net.Archeantus.Zombinator.Guns.Gun;

public class GunItem {
	public static ArrayList<GunItem> items = new ArrayList<GunItem>();
	
	private Gun gun;
	private int x, y;
	private Rectangle bounds;
	
	public Gun getGun(){return gun;}
	public int getX(){return x;}
	public int getY(){return y;}
	public Rectangle getBounds(){return bounds;}
	
	public GunItem(Gun g){
		this.gun = g;
		if(!g.isSecondary()){
			if(!(Zombinator.rows >= 1)){this.x = (((1280/2)-288)+(Zombinator.count*74))+32;}else{this.x = ((((1280/2)-288)+(Zombinator.count*74))+32);}
			this.y = (Zombinator.rows*74)+200;
		}
		else{
			if(!(Zombinator.trows >= 1)){this.x = (((1280/2)-288)+(Zombinator.tcount*74))+32;}else{this.x = ((((1280/2)-288)+(Zombinator.tcount*74))+32);}
			this.y = (Zombinator.trows*74)+200;
		}
		bounds = new Rectangle(x, y, 60, 60);
		items.add(this);
		if(!g.isSecondary()){
			Zombinator.count++;
			if(Zombinator.count >= 7){Zombinator.count = 0;Zombinator.rows++;}
		}
		else{
			Zombinator.tcount++;
			if(Zombinator.tcount >= 7){Zombinator.tcount = 0;Zombinator.trows++;}
		}
	}
}
