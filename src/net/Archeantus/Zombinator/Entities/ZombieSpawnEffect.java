package net.Archeantus.Zombinator.Entities;

import java.util.ArrayList;

public class ZombieSpawnEffect {
	
	public static ArrayList<ZombieSpawnEffect> getSpawning = new ArrayList<ZombieSpawnEffect>();
	
	private boolean isAlive;
	private int frCount = 0, frDelay = 3, frame = 0, frames = 8;
	private int x, y, direction;
	
	public void setX(int i){this.x = i;}
	public void setY(int i){this.y = i;}
	public int getX(){return this.x;}
	public int getY(){return this.y;}
	public int getFrame(){return frame;}
	public int getDirection(){return direction;}
	public boolean EntityIsAlive(){return isAlive;}
	public void updateFrame(){frCount++;if(frCount > frDelay){frCount = 0;frame++;if(frame > frames){frame = 0;remove();addZombie();}}}
	public void remove(){this.isAlive = false;}
	public void addZombie(){new Zombie(getX(), getY(), getDirection());}
	
	public ZombieSpawnEffect(int x, int y, int dir){
		setX(x);
		setY(y);
		this.isAlive = true;
		this.direction = dir;
		getSpawning.add(this);
	}
}
