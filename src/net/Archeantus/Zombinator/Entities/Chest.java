package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;

import net.Archeantus.Zombinator.Audio.Audio;

public class Chest {
	private int x, y, frame = 0, framemax = 5, framecount = 0, framedelay = 3;
	private Rectangle bounds;
	private boolean isOpening = false, isOpen = false, isClosing = false;
	
	public int getX(){return this.x;}
	public int getY(){return this.y;}
	public int getFrame(){return this.frame;}
	public boolean isOpen(){return isOpen;}
	public boolean isOpening(){return isOpening;}
	public boolean isClosing(){return isClosing;}
	public Rectangle getBounds(int offset){bounds = new Rectangle(getX()-offset, (600+getY())-58, 108, 58);return bounds;}
	public void setOpening(boolean i){this.isOpening  = i;}
	public void toggle(){if(isOpen()){isClosing = true;Audio.playStoredSound("chest_close", 0.5, 0);isOpen = false;}else{isOpening = true;Audio.playStoredSound("chest_open", 0.5, 0);}}
	public void updateFrame(){
		if(isOpening()){
			framecount++;
				
			if(framecount > framedelay){
				framecount = 0;
				frame++;
				
				if(frame >= framemax){
					this.isOpen = true;
					this.isOpening = false;
				}
			}
		}
		else if(isClosing()){
			framecount++;
			
			if(framecount > framedelay){
				framecount = 0;
				frame--;
				
				if(frame <= 0){
					this.isOpen = false;
					this.isClosing = false;
				}
			}
		}
	}
	
	public Chest(int x, int y){
		this.x = x;
		this.y = y;
	}
}
