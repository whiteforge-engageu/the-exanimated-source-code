package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;
import java.util.ArrayList;

import net.Archeantus.Zombinator.Util.Direction;

public class Zombie extends BasicEntity{
	
	public static ArrayList<Zombie> zombies = new ArrayList<Zombie>();
	
	private int frCount = 0, frDelay = 4, frame = 0, frames = 3, boxX = 0;
	private Rectangle bounds;
	private Direction dir = new Direction();
	private boolean isMoving;
	
	public Rectangle getBounds(){bounds = new Rectangle(boxX-20, getY()-80, 40, 80);return bounds;}
	public int getDirection(){return dir.getDirection();}
	public int getFrame(){return frame;}
	public int getBox(){return boxX;}
	public boolean isMoving(){return isMoving;}

	public void setDirection(int i){dir.setDirection(i);}
	public void setMoving(boolean s){this.isMoving = s;}
	public void setBox(int i){this.boxX = i;}
	public void updateFrame(){frCount++;if(frCount > frDelay){frCount = 0;frame++;if(frame > frames){frame = 0;}}}
	public void moveRight(){setMoving(true);setDirection(Direction.RIGHT);}
	public void moveLeft(){setMoving(true);setDirection(Direction.LEFT);}
	public void stopMoving(){setMoving(false);}
	
	public Zombie(int x, int y, int d){
		setX(x);
		setY(y);
		setDirection(d);
		setAlive(true);
		setSpeed(2);
		setMaxHealth(10);
		setHealth(10);
		setMoving(true);
		zombies.add(this);
	}
}
