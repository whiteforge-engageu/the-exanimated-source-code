package net.Archeantus.Zombinator.Entities;

import java.util.ArrayList;

public class DustEffect {
	public static ArrayList<DustEffect> dust = new ArrayList<DustEffect>();
	
	private boolean isAlive;
	private int frCount = 0, frDelay = 0, frame = 0, frames = 3;
	private int x, y;
	
	public void setX(int i){this.x = i;}
	public void setY(int i){this.y = i;}
	public int getX(){return x;}
	public int getY(){return y;}
	public int getFrame(){return frame;}
	public boolean EntityIsAlive(){return isAlive;}
	public void updateFrame(){frCount++;if(frCount > frDelay){frCount = 0;frame++;if(frame > frames){frame = 0;remove();}}}
	public void remove(){this.isAlive = false;}
	
	public DustEffect(int x, int y){
		setX(x);
		setY(y);
		isAlive = true;
		dust.add(this);
	}
}
