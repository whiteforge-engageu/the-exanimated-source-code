package net.Archeantus.Zombinator.Entities;

public class BasicEntity {
	
	private int x, y;
	protected int health;
	private int maxhealth;
	private int vel;
	private boolean isAlive;
	protected boolean flashed = false;
	
	public void setX(int x){this.x = x;}
	public void setY(int y){this.y = y;}
	public void setMaxHealth(int i){this.maxhealth = i;}
	public void setHealth(int i){this.health = i;}
	public void harm(int h){this.health -= h;flashed = true;}
	public void setSpeed(int speed){this.vel = speed;}
	public void setDead(){this.isAlive = false;}
	public void setAlive(boolean b){this.isAlive = b;}
	public boolean flashed(){return flashed;}
	
	public int getX(){return x;}
	public int getY(){return y;}
	public int getHealth(){return health;}
	public int getMaxHealth(){return maxhealth;}
	public int getSpeed(){return vel;}
	public boolean isAlive(){return isAlive;}
}
