package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;
import java.util.ArrayList;

public class Structure {
	public static ArrayList<Structure> structures = new ArrayList<Structure>();
	
	private int x, y, width, height, type;
	private Rectangle bounds;
	
	public int getX(){return this.x;}
	public int getY(){return this.y;}
	public int getWidth(){return this.width;}
	public int getHeight(){return this.height;}
	public int getType(){return this.type;}
	public Rectangle getBounds(int offset){bounds = new Rectangle(getX()-offset, (600+getY())-getHeight(), width, height);return bounds;}
	
	public Structure(int x, int y, int w, int h, int t){
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
		this.type = t;
		structures.add(this);
	}
}
