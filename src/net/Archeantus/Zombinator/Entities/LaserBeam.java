package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;
import java.util.ArrayList;

import net.Archeantus.Zombinator.Util.Direction;

public class LaserBeam {
	
	public static ArrayList<LaserBeam> bullets = new ArrayList<LaserBeam>();
	
	private int x, y, vel, dam, dir, width = 14, height = 13;
	private boolean alive;
	private Rectangle bounds;
	
	public void setX(int i){this.x = i;}
	public void setY(int i){this.y = i;}
	public void setVel(int i){this.vel = i;}
	public void setDamage(int i){this.dam = i;}
	public void setDirection(int i){this.dir = i;}
	public void setAlive(boolean i){this.alive = i;}
	
	public int getX(){return x;}
	public int getY(){return y;}
	public int getVelocity(){return vel;}
	public int getDamage(){return dam;}
	public int getDirection(){return dir;}
	public boolean EntityIsAlive(){return alive;}
	public int getWidth(){return width;}
	public int getHeight(){return height;}
	public Rectangle getBounds(){
		if(getDirection() == Direction.LEFT){
			bounds = new Rectangle(getX(), getY(), width, height);return bounds;
		}
		else{
			bounds = new Rectangle(getX()-width, getY()-height, width, height);return bounds;
		}
	} 
	
	public LaserBeam(int x, int y, int d, int dir){
		setX(x);
		setY(y);
		setVel(30);
		setDamage(d);
		setDirection(dir);
		setAlive(true);
		bullets.add(this);
	}
	
}
