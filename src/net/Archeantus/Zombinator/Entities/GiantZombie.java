package net.Archeantus.Zombinator.Entities;

import java.awt.Rectangle;
import java.util.ArrayList;

import net.Archeantus.Zombinator.Util.Direction;

public class GiantZombie extends BasicEntity{
public static ArrayList<GiantZombie> zombies = new ArrayList<GiantZombie>();
	
	private int frCount = 0, frDelay = 6, frame = 0, frames = 3, boxX = 0;
	private Rectangle bounds;
	private Direction dir = new Direction();
	private boolean isMoving;
	
	public Rectangle getBounds(){bounds = new Rectangle(boxX-44, getY()-104, 44, 104);return bounds;}
	public int getDirection(){return dir.getDirection();}
	public int getFrame(){return frame;}
	public int getBox(){return boxX;}
	public boolean isMoving(){return isMoving;}

	public void setDirection(int i){dir.setDirection(i);}
	public void setMoving(boolean s){this.isMoving = s;}
	public void setBox(int i){this.boxX = i;}
	public void updateFrame(){frCount++;if(frCount > frDelay){frCount = 0;frame++;if(frame > frames){frame = 0;}}}
	public void moveRight(){setMoving(true);setDirection(Direction.RIGHT);}
	public void moveLeft(){setMoving(true);setDirection(Direction.LEFT);}
	public void stopMoving(){setMoving(false);}
	
	public GiantZombie(int x, int y, int d){
		setX(x);
		setY(y);
		setDirection(d);
		setAlive(true);
		setSpeed(1);
		setMaxHealth(20);
		setHealth(20);
		setMoving(true);
		zombies.add(this);
	}
}
